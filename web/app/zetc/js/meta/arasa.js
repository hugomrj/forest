

/* global loader, ajax */

let arasa = 
{      
    
    
    dom:{
        main:"",
    },



    tabla: {
    
        json:  "",
        campos: ['uno', 'dos'],
        etiquetas: ['uno', 'dos', 'tres', 'cuatro','cinco'],
        html: "",
        linea:"",
        tbody_id:"",
        id:"",
        oculto: [],


        ini: function(obj) {        

            arasa.tabla.id = obj.tipo+ "-tabla";
            arasa.tabla.linea = obj.campoid;
            arasa.tabla.tbody_id = obj.tipo+"-tb";
            arasa.tabla.campos = obj.tablacampos;

            arasa.tabla.etiquetas = obj.etiquetas;
        },


        get_html: function(    ) {
            
            var oJson = JSON.parse(arasa.tabla.json) ;                                                        
            
            arasa.tabla.html = "";         

            for(x=0; x < oJson.length; x++) {     

                arasa.tabla.html += "<tr data-linea_id=\""+
                        oJson[x][arasa.tabla.linea]
                        +"\">";                  

                arasa.tabla.campos.forEach(function (elemento, indice, array) {                                

                    try { 
                      arasa.tabla.html += "<td data-title=\""+arasa.tabla.etiquetas[indice]+"\">";                    
                    }
                    catch (e) {
                        arasa.tabla.html += "<td>";     
                    }                

                    //tabla.html += "<td data-title=\""+tabla.etiquetas[indice]+"\">";                    
                    var jsonprop;

                    if (Array.isArray( elemento )){
                        jsonprop = arasa.tabla.campos_array(elemento, oJson[x]);
                    }
                    else
                    {
                        try {
                            eval("jsonprop = oJson[x]."+ elemento + ";");    
                        }
                        catch(error) {                        
                            jsonprop = "";        
                        }
                    }
                    arasa.tabla.html += jsonprop;     
                    arasa.tabla.html += "</td>";
                });    
                arasa.tabla.html += "</tr>";    
            }
            return arasa.tabla.html;
        },



        refresh: function( obj, page, buscar, fn  ) {      
            obj.lista(page); 
        }, 



        campos_array: function(miArray, json) {

            var ret = "";

            for (var i = 0; i < miArray.length; i+=1) {          
              eval("ret = ret +' '+json."+ miArray[i] + ";");              
            }        
            return ret;
        },    




        gene: function() {       
            document.getElementById( arasa.tabla.tbody_id ).innerHTML = arasa.tabla.get_html(); 
        },




/*
        limpiar: function() {     
            if (document.getElementById( tabla.tbody_id ))
            {
                tabla.json = '[]';
                tabla.gene();    
            }        
        },
*/



        lista_registro: function( obj ) {

            var tmptable = document.getElementById( arasa.tabla.id ).getElementsByTagName('tbody')[0];
            var rows = tmptable.getElementsByTagName('tr');

            for (var i=0 ; i < rows.length; i++)
            {
                rows[i].onclick = function()
                {   
                    var linea_id = this.dataset.linea_id;                                        
                    obj.tabla_registro(linea_id)
         
                };       
            }        
        },



/*
        set: {          
            tbodyid: function( objeto  ) {
                tabla.tbody_id = objeto.tipo+ "-tb";
            },
            tablaid: function( objeto  ) {
                tabla.id = objeto.tipo+ "-tabla";
            },
        },     
 */



/*
        ocultar: function() {

            var tableHe = document.getElementById( tabla.id ).getElementsByTagName('thead')[0];
            var rows = tableHe.getElementsByTagName('tr');

            tabla.oculto.forEach(function (ele, indice, array) {                        
                for (var i=0 ; i < rows.length; i++)
                {
                    cell = tableHe.rows[i].cells[ele] ;                                                  
                    cell.style.display = "none";
                }                           
            });    



            var tableBo = document.getElementById( tabla.id ).getElementsByTagName('tbody')[0];
            var rows = tableBo.getElementsByTagName('tr');

            tabla.oculto.forEach(function (ele, indice, array) {                        
                for (var i=0 ; i < rows.length; i++)
                {
                    cell = tableBo.rows[i].cells[ele] ;                                                  
                    cell.style.display = "none";
                }                           
            });    

        },
*/


        formato: function( obj ) {

            try { 

                var table = document.getElementById( arasa.tabla.id  ).getElementsByTagName('tbody')[0];
                var rows = table.getElementsByTagName('tr');

                for (var i=0 ; i < rows.length; i++)
                {

                    for (var j=0 ; j < obj.tablaformat.length; j++)
                    {
                        
                        var type= obj.tablaformat[j];
                     
                        switch(type) {

                            case 'C':                                                 
                                break;                        

                            case 'N':                                  

                                cell = table.rows[i].cells[j] ;                                  
                                cell.innerHTML = fmtNum(cell.innerHTML);                        
                                cell.style = "text-align: right";
                                break;

                                case 'M':                                  

                                cell = table.rows[i].cells[j] ;                                  
                                cell.innerHTML = fmtNum(cell.innerHTML);                        
                                /*cell.style = "text-align: right";*/
                                break;



                            case 'D':                                                 

                                cell = table.rows[i].cells[j] ;                                             
                                cell.innerHTML = fDMA (cell.innerHTML) ;                 

                                cell.style = "text-align: right";
                                break;                        

                            case 'R':                                                 

                                cell = table.rows[i].cells[j] ;                               
                                cell.style = "text-align: right";
                                break;                        


                            case 'Z':           

                                cell = table.rows[i].cells[j] ;                                                 

                                if (cell.innerHTML === 'undefined'){
                                    cell.innerHTML = "";
                                }
                                else{

                                    cell.innerHTML = fmtNum(cell.innerHTML);                        
                                    cell.style = "text-align: right";                                
                                }
                                break;                                                    


                            case 'U':           

                                cell = table.rows[i].cells[j] ;                                                 

                                if (cell.innerHTML === 'undefined'){
                                    cell.innerHTML = "";
                                }
                                break;                                                    

                            case 'E':           

                                cell = table.rows[i].cells[j] ;                                
                                cell.innerHTML = "";
                                cell.style = "border: none";                                

                                break;                            

                            default:
                                //code block
                        }      
                    }
                }
            }
            catch (e) {  
                console.log(e);
            }       

        },


    },












    html:{
        

        menu: {  

            user: 0,
            div: "menubar",

            ini: function( ) {                 

                ajax_2.url = html.url.absolute() + "/api/selectores/menu";           
                ajax_2.metodo = "GET";     
                arasa.html.menu.user = ajax_2.private.json(null);

                var mn = ajax_2.req.getResponseHeader("menu");

                localStorage.setItem('menu', mn );   

            },                                



            mostrar: function(  ) {            

                 var nomsis = arasa.html.url.absolute();

                 if (nomsis != "/webapp"){
                     tmpmenu = localStorage.getItem('menu'); 
                     tmpmenu = tmpmenu.replaceAll('/webapp', nomsis);

                     localStorage.setItem('menu', tmpmenu );     
                 }


                document.getElementById( arasa.html.menu.div ).innerHTML 
                        =  localStorage.getItem('menu');                    

                //  logo   header  <img src=\"../../ima/header/01.png\"/>

                document.getElementById( "header_logo" ).innerHTML  =
                        "<div></div>\n\
                        <div></div>\n\
                        <div></div>";   
            },    


        },

        
            
        url:{
            
            redirect: function(val) 
            {
                
//alert(modsis)   
                var modsis =  localStorage.getItem('modsis');

    /*                                   
                var URLactual = window.location.href;
                
                var expression = "smbv/";
                var index = URLactual.search(expression);
                if(index == 0) {
                    expression = "";
                } 
*/


                switch(val) {
                    case 200:
                        break;                    
                    case 401:
                        window.location = arasa.html.url.absolute()+"/"+modsis;                                     
                    case 500:
                        window.location = arasa.html.url.absolute()+"/"+modsis ;                 
                        break;            
                    default:
                        window.location = arasa.html.url.absolute()+"/"+modsis ;                 
                }

            },              
            
            
            
            control_promise: function() 
            {

                const promise = new Promise((resolve, reject) => {
            
                                        
                    arasa.loader.inicio();

                    var comp = window.location.pathname;                            
                    var path =  comp.replace(arasa.html.url.absolute() , "");       

                    var xhr =  new XMLHttpRequest();            
                    var url = arasa.html.url.absolute() +"/api/usuarios/controlurl"; 
                    var metodo = "GET";                         

                    
                    xhr.open( metodo.toUpperCase(),   url,  true );      
                    
                    
                    xhr.onreadystatechange = function () {
                        if (this.readyState == 4 ){

                            //ajax.headers.get();
                            
                            arasa.ajax.local.token =  xhr.getResponseHeader("token") ;            
                            localStorage.setItem('token', xhr.getResponseHeader("token") );     

                            arasa.ajax.state = xhr.status;
                            
                                resolve( xhr );

                            //arasa.html.url.redirect(ajax.state);  
                            //arasa.html.barra_superior(xhr);  
                
                            arasa.loader.fin();

                        }
                    };
                    xhr.onerror = function (e) {                    
                        reject( xhr );                 
                    };                       

                    xhr.setRequestHeader("path", path );
                    var type = "application/json";
                    xhr.setRequestHeader('Content-Type', type);   
                    //ajax.headers.set();
                    xhr.setRequestHeader("token", localStorage.getItem('token'));           

                    xhr.send( null );                       
                   
                    
 
                })

                return promise;


              },

            
            
            
        
            absolute: function() 
            {
                var pathname = window.location.pathname;
                pathname = pathname.substring(1,pathname.length);
                pathname = pathname.substring(0,pathname.indexOf("/"));
                return "/"+pathname ;            
            },               

        
        
        },
        



        barra_superior: function( xhr ) {  

            ajax.url = arasa.html.url.absolute()  + '/basicas/barrasuperior/barrasuperior.html';   
            ajax.metodo = "GET";            
            document.getElementById( "barrasuperior" ).innerHTML =  ajax.public.html();        

            document.getElementById( "session_usuario_cab" ).innerHTML = 
                    xhr.getResponseHeader("sessionusuario");

            document.getElementById( "session_sucursal_cab" ).innerHTML = 
                    xhr.getResponseHeader("sessionsucursal");    

        },





        paginacion: {  
            
            pagina:     1,     
    
            total_registros: 0,

            lineas:       12,
            cantidad_paginas:  0,

            pagina_inicio : 1,
            pagina_fin : 0, 
            bloques:    12,

            html: "",    

            ini: function(json) { 
                
                var ojson = JSON.parse(json) ;                                     

                
                            
                arasa.html.paginacion.total_registros = ojson["total_registros"];
                                
                var pagina = ojson["pagina"];
                                

                var cantidad_paginas;
                var total_registros = arasa.html.paginacion.total_registros ;
                var lineas = arasa.html.paginacion.lineas;
                var bloques = arasa.html.paginacion.bloques;
                var pagina_inicio = arasa.html.paginacion.pagina_inicio;
                var pagina_fin = arasa.html.paginacion.pagina_fin;
                


                cantidad_paginas = total_registros / lineas;
                cantidad_paginas = Math.trunc(cantidad_paginas);

                var calresto  = cantidad_paginas * lineas;
                if (calresto != total_registros) {
                    cantidad_paginas = cantidad_paginas + 1;
                }


                if ((cantidad_paginas * lineas ) < total_registros){
                    cantidad_paginas++;
                }        

                if (bloques > cantidad_paginas) {
                    pagina_inicio = 1;
                    pagina_fin = cantidad_paginas;
                }        
                else{
                    if (bloques == cantidad_paginas) {
                        pagina_inicio = 1;
                        pagina_fin = bloques;
                    }    
                    else{ 
                        if (bloques  < cantidad_paginas) {

                            if (pagina - (bloques / 2) < 1) {
                                pagina_inicio = 1;
                                pagina_fin = pagina + bloques;
                            }
                            else{
                                pagina_inicio = pagina - (bloques / 2);
                            }

                            if (pagina + (bloques / 2) > cantidad_paginas )                        {
                                pagina_inicio = cantidad_paginas - bloques;
                                pagina_fin = cantidad_paginas;
                            }
                            else{
                                pagina_fin =  pagina + (bloques / 2);
                            }    
                        }
                    }    
                }                 

                arasa.html.paginacion.pagina = pagina;
                arasa.html.paginacion.cantidad_paginas = cantidad_paginas;
                arasa.html.paginacion.total_registros = total_registros;
                arasa.html.paginacion.pagina_inicio = pagina_inicio;
                arasa.html.paginacion.pagina_fin = pagina_fin;


            },


            gene: function() {       

                //paginacion.total_registros  = 16
        
                var total_registros = arasa.html.paginacion.total_registros ;
                var cantidad_paginas = arasa.html.paginacion.cantidad_paginas ;
                var pagina = arasa.html.paginacion.pagina ;
                var pagina_inicio = arasa.html.paginacion.pagina_inicio ;
                var pagina_fin = arasa.html.paginacion.pagina_fin;


                var html;

                if (total_registros == 0){
                    return "";
                }

                if (cantidad_paginas == 1){
                    pagina = 1;
                }

                html = "<ul data-paginaactual=\"1\" id=\"paginacion\">";


                if (pagina != pagina_inicio){            
                    html += "<li data-pagina=\"ant\" >" ;

                    html +=      "<a href=\"javascript:void(0);\"id =\"ant\">" ;
                    html +=          "anterior" ;
                    html +=      "</a>" ;

                    html += "</li>";
                }



                for (i = pagina_inicio; i <= pagina_fin; i++) {   

                    if (pagina === i)
                    {                
                        html += "<li class=\"actual\" data-pagina=\"act\">" ;
                        html += "<a href=\"javascript:void(0);\" > " ;
                        html += i ;
                        html += "</a>" ;
                        html += "</li>" ;
                    }
                    else
                    {                
                        html += "<li data-pagina= \"pag" + i + "\"  >" ;
                        html += "<a href=\"javascript:void(0);\" id =pag"+ i +"> " ;
                        html += i ;
                        html += "</a>" ;
                        html += "</li>" ;                

                    }
                }

                if (pagina != pagina_fin){            
                    html += "<li data-pagina=\"sig\" >" ;

                    html +=      "<a href=\"javascript:void(0);\"id =\"sig\">" ;
                    html +=          "siguiente" ;
                    html +=      "</a>" ;

                    html += "</li>";
                }
                html += " </ul>"
                return html;

            },






            move: function(  obj, busca, fn ) {       


                var listaUL = document.getElementById( obj.tipo + "_paginacion" );
                var uelLI = listaUL.getElementsByTagName('li');
                
                
                var pagina = 0;
                
                
                for (var i=0 ; i < uelLI.length; i++)
                {
                    var lipag = uelLI[i];   
                    
                    if (lipag.dataset.pagina == "act"){                                     
                        pagina = lipag.firstChild.innerHTML;
                    }                    
                }
                


                for (var i=0 ; i < uelLI.length; i++)
                {
                    var datapag = uelLI[i].dataset.pagina;     

                    if (!(datapag == "act"  || datapag == "det"  ))
                    {
                        uelLI[i].addEventListener ( 'click',
                            function() {                                      

                                switch (this.dataset.pagina)
                                {
                                   case "sig": 
                                           pagina = parseInt(pagina) +1;
                                           break;                                                                          

                                   case "ant":                                     
                                           pagina = parseInt(pagina) -1;
                                           break;

                                   default:  
                                           pagina = this.childNodes[0].innerHTML.toString().trim();
                                           break;
                                }
                                pagina = parseInt( pagina , 10);                                                                       
                                
                                //arasa.html.paginacion.pagina = pagina;
                                
                                arasa.tabla.refresh( obj, pagina, busca, fn  ) ;

                            },
                            false
                        );                
                    }            
                }           

            },

        },
 
    },
                    






    vista:{
    
        lista_paginacion: function( obj, page ) {


            const promise = new Promise((resolve, reject) => {

                arasa.loader.inicio();
                obj.page = page;

                var comp = window.location.pathname;                 
                var path =  comp.replace( arasa.html.url.absolute() , "");

                var xhr =  new XMLHttpRequest();           
                

                var filtro = ""        
                if(typeof obj.getUrlFiltro === 'function') {
                  filtro = obj.getUrlFiltro(obj);
                }                    
               
                
                var url = arasa.html.url.absolute() +"/api/"+obj.recurso; 
                url = url + filtro;
                url = url + "?page=" + obj.page;
                

                var metodo = "GET";                         
                arasa.ajax.json  = null;
                xhr.open( metodo.toUpperCase(),   url,  true );      

                xhr.onreadystatechange = function () {
                    if (this.readyState == 4 ){

                        //let promesa = arasa.vista.tabla_html(obj);
                        //promesa       

                        arasa.vista.tabla_html(obj)
                            .then(( text ) => {

                                // botones de accion - nuevo para este caso

                                arasa.ajax.local.token =  xhr.getResponseHeader("token") ;            
                                localStorage.setItem('token', xhr.getResponseHeader("token") );     
                                //sessionStorage.setItem('total_registros',  ajax.xhr.getResponseHeader("total_registros"));


                                var ojson = JSON.parse( xhr.responseText ) ;     
                                tabla.json = JSON.stringify(ojson['datos']) ;


                                tabla.ini(obj);
                                tabla.gene();   
                                tabla.formato(obj);

                                tabla.set.tablaid(obj);     
                                tabla.lista_registro(obj, reflex.form_id_promise ); 

                                var json_paginacion = JSON.stringify(ojson['paginacion']);
                                
                                arasa.vista.paginacion_html(obj, json_paginacion );

                                arasa.ajax.state = xhr.status;

                                    resolve( xhr );

                                arasa.loader.fin();

                            })
                        
                        //arasa.vista.tabla_html(obj);
                    }
                };
                xhr.onerror = function (e) {                    
                    reject(
                          xhr.status,
                          xhr.response   
                    );                 

                };                       

                xhr.setRequestHeader("path", path );
                var type = "application/json";
                xhr.setRequestHeader('Content-Type', type);   

                //ajax.headers.set();
                xhr.setRequestHeader("token", localStorage.getItem('token'));           

                xhr.send( arasa.ajax.json  );                       


            })

            return promise;

        },



        

        paginacion_html: function( obj , json ) {   

            arasa.html.paginacion.ini(json);

            document.getElementById( obj.tipo + "_paginacion" ).innerHTML 
                    = arasa.html.paginacion.gene();   

            arasa.html.paginacion.move(obj, "", reflex.form_id);

        },





        tabla_html: function( obj ) {   

            const promise = new Promise((resolve, reject) => {

                if (!(obj.alias === undefined)) {    
                    arasa.ajax.url = html.url.absolute() + obj.carpeta +'/'+ obj.alias + '/htmf/lista.html';    
                }              
                else{
                    arasa.ajax.url = html.url.absolute() + obj.carpeta +'/'+ obj.tipo + '/htmf/lista.html';    
                }


                fetch( arasa.ajax.url )
                  .then(response => {
                    return response.text();
                  })
                  .then(data => {
                                      

                    document.getElementById( arasa.dom.main ).innerHTML =  data;
                    reflex.getTituloplu(obj);                                  
                        resolve( data );

                  })
                  
            })

            return promise;


        },


        
        
        
        
    },    
    


    modal:{
        

        paginacion_html: function( obj , json, busca ) {   

            arasa.html.paginacion.ini(json);

            document.getElementById( obj.tipo + "_paginacion" ).innerHTML 
                    = arasa.html.paginacion.gene();   

            arasa.modal.paginacion_move(obj, busca);
            

        },



        paginacion_move: function(  obj, busca ) {       

            var listaUL = document.getElementById( obj.tipo + "_paginacion" );
            var uelLI = listaUL.getElementsByTagName('li');

            var pagina = 0;

            for (var i=0 ; i < uelLI.length; i++)
            {
                var lipag = uelLI[i];   

                if (lipag.dataset.pagina == "act"){                                     
                    pagina = lipag.firstChild.innerHTML;
                }                    
            }



            for (var i=0 ; i < uelLI.length; i++)
            {
                var datapag = uelLI[i].dataset.pagina;     

                if (!(datapag == "act"  || datapag == "det"  ))
                {
                    uelLI[i].addEventListener ( 'click',
                        function() {                                      

                            switch (this.dataset.pagina)
                            {
                               case "sig": 
                                       pagina = parseInt(pagina) +1;
                                       break;                                                                          

                               case "ant":                                     
                                       pagina = parseInt(pagina) -1;
                                       break;

                               default:  
                                       pagina = this.childNodes[0].innerHTML.toString().trim();
                                       break;
                            }
                            pagina = parseInt( pagina , 10);                                                                       

                            //arasa.html.paginacion.pagina = pagina;
                            busqueda.modal.tablabusqueda(obj, pagina, busca);

                        },
                        false
                    );                
                }            
            }           

        },
       
        
        
        
        
    },    
    
    




    ajax:  {  

        server: '' ,  
        json: "",
        url: "",
        state: 0,    
        dataType: 'json',
        metodo: '',

        local:{
             token : ""
        },


        absolute: function() 
        {
            var pathname = window.location.pathname;
            pathname = pathname.substring(1,pathname.length);
            pathname = pathname.substring(0,pathname.indexOf("/"));
            return "/"+pathname ;            
        },    

        getserver: function() 
        {
            if (this.server == ""){
                return this.absolute()
            }
            else{
                return this.server;
            }           
        }, 






        promise: {               

            async : function ( metodoHtml ){    

                const promise = new Promise((resolve, reject) => {

                    arasa.loader.inicio();

                    var xhr =  new XMLHttpRequest();            
                    var url = arasa.ajax.api;

                    xhr.open( metodoHtml.toUpperCase(),   url,  true );      

                    xhr.onreadystatechange = function () {
                        if (this.readyState == 4 ){

                            arasa.loader.fin();
                            arasa.ajax.state = xhr.status;
                            resolve( xhr );

                        }
                    };
                    xhr.onerror = function (e) {                    
                        reject(
                              xhr.status,
                              xhr.response   
                        );                 

                    };                       

                    //xhr.setRequestHeader("path", path );
                    var type = "application/json";
                    xhr.setRequestHeader('Content-Type', type);   
                    //xhr.setRequestHeader("token", localStorage.getItem('token'));           

                    xhr.send( arasa.ajax.json  );     
                });

                return promise;
            },        



        },







        headers: {    

            setRequest: function( ) {                                  
                    ajax.req.setRequestHeader("token", localStorage.getItem('token'));
            },

            getResponse: function( ) {    

                ajax.local.token = ajax.req.getResponseHeader("token") ;            
                localStorage.setItem('token', ajax.local.token);     

                sessionStorage.setItem('total_registros',  ajax.req.getResponseHeader("total_registros"));


            },



            set: function( ) {                                  
                    ajax.xhr.setRequestHeader("token", localStorage.getItem('token'));
            },



            get: function( ) {    

                ajax.local.token = ajax.xhr.getResponseHeader("token") ;            
                localStorage.setItem('token', ajax.local.token);     

                sessionStorage.setItem('total_registros',  ajax.xhr.getResponseHeader("total_registros"));

            },




        },



        private:{    

            json: function( data ) {                                           

                ajax.req =  new XMLHttpRequest();
                ajax.json = data ;                      
                ajax.req.open( ajax.metodo.toUpperCase(),   ajax.url,  false );              
                ajax.req.setRequestHeader('Content-Type', 'application/json');   

                ajax.headers.setRequest();
                ajax.req.send( ajax.json );                        
                ajax.headers.getResponse();

                ajax.state = ajax.req.status;      
                return  ajax.req.responseText;
            },



            jasper: function() {         

                ajax.req =  new XMLHttpRequest();             
                var url = html.url.absolute() +  "/TokenReport";                        
                ajax.req.open( "POST", url, false);              
                ajax.req.setRequestHeader('Content-Type', 'application/html;charset=UTF-8');

                ajax.headers.setRequest();              
                ajax.req.send();                    
                ajax.headers.getResponse();

                ajax.state = ajax.req.status;          
                if (ajax.state == 202){                
                    window.open( ajax.url , '_blank');
                }
                //return ajax.req.responseText;               

            },                



        },




        public:{    

            html: function() {



                ajax.req =  new XMLHttpRequest(); 
                ajax.req.open( ajax.metodo.toUpperCase(), ajax.url, false);              
                ajax.req.setRequestHeader('Content-Type', 'text/html;charset=UTF-8');

    console.log("deprecated")
    console.log(ajax.url);

                ajax.req.send();  

                ajax.state = ajax.req.status;                    
                return ajax.req.responseText;



            },


        },





        async:{    

            get: function( ) {                                           


                const promise = new Promise((resolve, reject) => {

                        arasa.loader.inicio();
                        //obj.page = page;

                        var comp = window.location.pathname;                 
                        var path =  comp.replace( arasa.html.url.absolute() , "");

                        var xhr =  new XMLHttpRequest();      

                        var metodo = "GET";                         

                        xhr.open( metodo.toUpperCase(),   ajax.url,  true );      

                        xhr.onreadystatechange = function () {
                                if (this.readyState == 4 ){

                                    arasa.loader.fin();                                
                                    resolve( xhr );

                                }
                        };
                        xhr.onerror = function (e) {                    
                                reject(
                                    xhr.status,
                                    xhr.response   
                                );                 

                        };                       

                        xhr.setRequestHeader("path", path );
                        var type = "application/json";
                        xhr.setRequestHeader('Content-Type', type);   
                        xhr.setRequestHeader("token", localStorage.getItem('token'));           
                        xhr.send( arasa.ajax.json  );                       


                })

                return promise;



            },

        },

    },




    msg:  
    {    
        texto: "", 
        timeOutPeriod: 4000,
        divContenedor: "idalertas",

        generarUID: function() {        
            return "msg-" + Math.floor(Math.random() * 999999);
        },


        error : {        

            mostrar: function(mensaje) {

                var UID = arasa.msg.generarUID();
                arasa.msg.crear(UID, mensaje ).setAttribute("class", "alerta alerta-error "); ;        
                arasa.msg.eliminar(UID);
                return;            
            }
        },

        ok : {        

            mostrar: function(mensaje) {

                var UID = arasa.msg.generarUID();
                arasa.msg.crear(UID, mensaje ).setAttribute("class", "alerta alerta-ok "); ;        
                arasa.msg.eliminar(UID);
                return;            
            }
        },


        eliminar:  function(id) {

            var strCmd = "document.getElementById('"+this.divContenedor+"').removeChild(document.getElementById('"+id+"'));";

            setTimeout(strCmd, this.timeOutPeriod);
            return;        
        },


        crear:  function(uid, mensaje) {

            this.conten();
            var midiv = document.createElement("div");
            midiv.setAttribute("id", uid);        
            midiv.innerHTML = mensaje ;
            document.getElementById(this.divContenedor).appendChild(midiv);

            return midiv;        
        },


        conten:  function() {

            if ( (document.getElementById(this.divContenedor) === null)){

                var div = document.createElement("div");
                div.setAttribute("id", this.divContenedor);
                //div.innerHTML = "<div></div>";
                document.body.appendChild(div);
            }               
        }


    },







    form :
    {  
        name: "",
        json:  "",
        campos: [],


        llenar: function(  ) {


            var oJson = JSON.parse(arasa.form.json) ; 
            var campos = document.getElementById( arasa.form.name ).getElementsByTagName("input");    


            for(var i=0; i< campos.length; i++) {            
                var c = campos[i];                      

                    if (typeof(c.dataset.foreign) === "undefined") {

                        if (c.type == "text"  ||  c.type == "hidden"  )
                        {                


                            if (c.className == "num")
                            {
                                c.value  = oJson[c.name]; 
                            }
                            else
                            {  
                                //alert(oJson[campos[i].name] );
                                c.value  = oJson[c.name]; 

                                if (c.value === 'undefined') {
                                    c.value  = ""; 
                                }  
                            }                
                        }
                        else
                        {

                            var type= c.type;
                            switch(type) {

                                case "date":                                                 
                                    c.value  = jsonToDate( oJson[c.name] );   
                                    break;                        

                                case "password":                                  
                                    c.value  = oJson[c.name];                     
                                    break;

                                case "radio":                                  
                                    //c.value  = oJson[c.name];         
                                    arasa.form.radios.databool(c.name, oJson[c.name] );
                                    break;

                                case "checkbox":                                  
                                    //c.value  = oJson[c.name];         
                                    //form.radios.databool(c.name, oJson[c.name] );

                                    arasa.form.checkbox.databool(c, oJson[c.name] );


                                    break;


                                default:
                                    //code block
                            }                              



                        }            
                    }
                    else
                    {   

                        var objev = eval( "new "+c.dataset.foreign+"()");          

                        if (objev.objjson == undefined){
                            var f = c.dataset.foreign;
                        }
                        else{
                            var f = objev.objjson;
                        }

                        f = f.toLowerCase();

                        try {

                                c.value = oJson[f][objev.campoid] ;     

                                vardes = oJson[f][objev.json_descrip] ;

                                var foo = document.getElementById( objev.form_descrip ).nodeName;


                                switch (foo) {
                                  case "INPUT":
                                    document.getElementById( objev.form_descrip ).value =  vardes
                                    break;
                                  case "OUTPUT":            

                                    document.getElementById( objev.form_descrip ).innerHTML =  vardes
                                    break;
                                  default:
                                    console.log('default switch');
                                }                        

                            //console.log(document.getElementById( objev.form_descrip ).nodeName)           
                            //document.getElementById( objev.form_descrip ).innerHTML                                 =  vardes


                        }
                        catch(error) {

                                console.error(c.dataset.foreign);
                                console.error(error);
                                c.value = "0" ;              
                              //  document.getElementById( objev.form_descrip ).innerHTML = "";
                        }                                                  
                    }            



            }
            
            arasa.form.llenar_textarea(arasa.form.json);


        },







        llenar_cbx: function( obj ) {


            try {

                var oJson = JSON.parse(arasa.form.json) ;      


                var campos = document.getElementById( arasa.form.name ).getElementsByTagName("select");    


                for(var i=0; i< campos.length; i++) {   

                    var c = campos[i];       
                    var opt = document.createElement('option');            

                    if (!(campos[i].dataset.vista === "notGet" )){     

                        opt.value = oJson[c.name][obj.combobox[c.name]['value']];
                        opt.innerHTML = oJson[c.name][obj.combobox[c.name]['inner']];   
                        document.getElementById(   campos[i].id ).appendChild(opt);                               

                    }                        
                }
            }
            catch (e) {
                console.log(e)
                console.log(c.name)
            }        


        },






        llenar_textarea: function( json  ) {

            var oJson = JSON.parse(json) ;      

            var campos = document.getElementById( arasa.form.name ).getElementsByTagName("textarea");    

            for(var i=0; i< campos.length; i++) {               
                var c = campos[i];         
                    //c.value = "cargado;"
                     c.value  = oJson[c.name]; 
            }
        },








        relaciondescrip: function( id, obj ) {

            reflex.ini(obj);
            ajax_2.url = html.url.absolute() +'/api/' + obj.recurso + '/'+id;    

            ajax_2.metodo = "GET";

            arasa.form.json = ajax_2.private.json();  
            var camdes =  document.getElementById(obj.form_descrip);   


            if (arasa.ajax.state == 200){            

                var oJson = JSON.parse(arasa.form.json) ;    

                if (Array.isArray(obj.json_descrip))
                {
                    camdes.innerHTML = "";

                    obj.json_descrip.forEach(function(element) {
                      //console.log(element);
                      camdes.innerHTML = camdes.innerHTML + oJson[element] + " ";                   
                    });                                

                }
                else
                {



                    //camdes.innerHTML = oJson[obj.json_descrip];                   
                }

            }
            else{
                camdes.innerHTML = "";                                
            }


        },





        disabled: function( bool ) {


            var fcampos = document.getElementById( arasa.form.name ).getElementsByTagName("input");    

            var esIgual = false;    
            for(var i=0; i< fcampos.length; i++) 
            {            

                arasa.form.campos.forEach(function (elemento, indice, array) {

                    if (elemento === fcampos[i].id ){                                        
                        esIgual = true;
                    }

                });        


                if (!(fcampos[i].type == 'radio'))
                {
                    // en caso que sea check
                    //if (fcampos[i].type == 'checkbox') {                
                    if (false) {                
                    }
                    else{
                        if (!( esIgual )){
                            document.getElementById(fcampos[i].id).disabled =  !bool;
                        }else{
                            document.getElementById(fcampos[i].id).disabled = bool;                
                        }
                        esIgual = false;  
                    }

                }
                else
                {
                    arasa.form.radios.disable (fcampos[i].name, !bool);
                }


            }




            var fcampos = document.getElementById( arasa.form.name ).getElementsByTagName("select");            
            var esIgual = false;

            for(var i=0; i< fcampos.length; i++) 
            {            
                arasa.form.campos.forEach(function (elemento, indice, array) {    
                    if (elemento === fcampos[i].id ){                                        
                        esIgual = true;
                    }
                });                

                if (!( esIgual )){
                    document.getElementById(fcampos[i].id).disabled =  !bool;
                }else{
                    document.getElementById(fcampos[i].id).disabled = bool;                
                }
                esIgual = false;            
            }




            var fcampos = document.getElementById( arasa.form.name ).getElementsByTagName("textarea");            
            var esIgual = false;

            for(var i=0; i< fcampos.length; i++) 
            {            
                arasa.form.campos.forEach(function (elemento, indice, array) {    
                    if (elemento === fcampos[i].id ){                                        
                        esIgual = true;
                    }
                });                

                if (!( esIgual )){
                    document.getElementById(fcampos[i].id).disabled =  !bool;
                }else{
                    document.getElementById(fcampos[i].id).disabled = bool;                
                }
                esIgual = false;            
            }

            arasa.form.campos = [];

        },







        ocultar_foreign: function(  ) {

            var iconclass = document.getElementById( arasa.form.name ).getElementsByClassName("icono");        

            for(var i=0; i< iconclass.length; i++) {        

                iconclass[i].style.display = "none";   

                //iconclass[i].parentNode.parentNode.style.width = "10px";   

                //iconclass[i].parentNode.style.width = "10px";   

            }            

        },







        mostrar_foreign: function(  ) {

            var iconclass = document.getElementById( arasa.form.name ).getElementsByClassName("icono");        
            for(var i=0; i< iconclass.length; i++) {              
                iconclass[i].style.display = "flex";   
                iconclass[i].parentNode.parentNode.style.width = "auto";   
                //iconclass[i].parentNode.style.width = "auto";   
            }            
        },    





        datos: {   

            getjson: function( ) {    

                var campos = document.getElementById( arasa.form.name ).getElementsByTagName("input");  

                var arr = [];            
                var str = "";        

                for (var i=0; i< campos.length; i++) {         


                    // control d elementos no repetidos  // no funnciona              
                    var idx = arr.indexOf(campos[i].name);

                    if (idx == -1){

                        if (!(typeof campos[i].dataset.vista === "notGet" )){     
                            arr.push(campos[i].name);    
                        }     
                    }
                    else{
                        break;
                    }




                    var ele = campos[i];                   

                        var type= ele.type;
                        switch(type) {

                            case "hidden":                                                 

                                if (typeof ele.dataset.pertenece === "undefined" ){     
                                    str =  str  + "";
                                }  
                                else
                                {                                
                                    if (str  != ""){
                                        str =  str  + ",";
                                    }                                                
                                    str =   str + arasa.form.datos.elemetiq(campos[i]) ;                           
                                }                            
                                break;                        


                            case "radio":                                                 

                                if (str  != ""){
                                    str =  str  + ",";
                                }   

                                str =   str + arasa.form.radios.getdatabol(ele.name );


                                break;                        


                            case "checkbox":                                                 

                                if (str  != "") {
                                    str =  str  + ",";
                                }   

                                //str =   str + arasa.form.radios.getdatabolcheck(ele );
                                str =   str + arasa.form.checkbox.getdatabool(ele);

                                break;                        

                            default:

                                if (typeof ele.dataset.vista === "undefined" ){    
                                    if (str  != ""){
                                        str =  str  + ",";
                                    }                                                

                                    str =   str + arasa.form.datos.elemetiq(campos[i]) ;                    
                                }    

                        }                       

                    }





                var str2 = "";  
                var combos = document.getElementById( arasa.form.name ).getElementsByTagName("select");   

                for (var y=0; y< combos.length; y++) {               

                    if (!(combos[y].dataset.vista === "notGet" )) {     
                        if (str2  != ""){
                            str2 =  str2  + ",";
                        }                                                            
                        str2 =  str2 + arasa.form.datos.elemetcombo(combos[y]) ;    
                    }                          

                }

                if (str2  != ""){
                    if (str  != ""){
                        str =  str  + "," + str2;
                    }
                    else{
                        str =  str2;
                    }
                }



                // textarea
                var str3 = "";  
                var areas = document.getElementById( arasa.form.name ).getElementsByTagName("textarea");   
                for (var x=0; x< areas.length; x++) {                

                    if (str3  != ""){
                        str3 =  str3  + ",";
                    }                                                            
                    str3 =  str3 + arasa.form.datos.elemenTextArea(areas[x]);                      
                }

                if (str3  != ""){
                    if (str  != ""){
                        str =  str  + "," + str3;
                    }
                    else{
                        str =  str3;
                    }
                }

                return "{" +str+ "}"  ;            
            },   


            elemen: function( ele ) {    


                var str = "";        

                str =  str  + "\"" +ele.getAttribute('name')+ "\"" ;
                str =   str + ":";            

                if (ele.type == "text"  ||  ele.type == "hidden"  )
                {  
                    if (ele.className == "num")
                    {
                        //str = str + ele.value  ;    
                        str = str + NumQP(ele.value)  ;    
                    }
                    else
                    {
                        str = str + "\"" +ele.value+ "\"" ;
                    }                
                }
                else
                {

                    if (ele.type == "password"){
                        str =   str + "\"" + ele.value+ "\"" ;
                    }
                    if (ele.type == "date"){


                        if (ele.value !=  "")
                        {  
                            str =   str + "\"" + ele.value+ "\"" ;
                        }
                        else
                        {
                            str =   str + " null " ;
                        }

                        //str =   str + "\"" + ele.value+ "\"" ;
                    }                                
                }

                return str ;            
            },



            elemenTextArea: function( ele ) {    

                var str = "";        

                str =  str  + "\"" +ele.getAttribute('name')+ "\"" ;
                str =   str + ":";            

                str = str + "\"" +ele.value+ "\"" ;

                return str ;            
            },




            elemetiq: function( ele ) {                


                var str = "";              

                if (typeof ele.dataset.foreign === "undefined" ){             

                    str =   str + arasa.form.datos.elemen(ele).toString();

                }
                else
                {


                    if (typeof ele.dataset.json === "undefined" ){             
                        var e = ele.dataset.foreign.toString().toLocaleLowerCase();
                    }                
                    else{
                        var e = ele.dataset.json.toString().toLocaleLowerCase();
                    }


                    if (e == ele.getAttribute('name') ){                    
                        //str =  str  + "\"" +e+ "\"" ;                
                        str =  str  + "\"" +ele.getAttribute('name')+ "\"" ;                

                    }
                    else{                    
                        str =  str  + "\"" +e+ "\"" ;                
                        //str =  str  + "\"" +ele.getAttribute('name')+ "\"" ;                
                    }


                    //str =  str  + "\"" +ele.dataset.foreign+ "\"" ;                
                    str =   str + ":";                  
                    str =  str + "{ "+  arasa.form.datos.elemen(ele) + " }" ;
                }           
                return str ;            
            },


            elemetcombo: function( ele ) {      


                var str = "";              

                if (typeof ele.dataset.foreign === "undefined" ){         
                    str = str + " \"" +ele.name+ "\" " ;            
                }

                else{                
                    var e = ele.dataset.foreign.toString().toLocaleLowerCase();
                    str = str + " \"" + e + "\"" ;            
                } 

                str =   str + ":";            
                str =  str + "{ ";  
                str =  str + " \"" +ele.name+ "\": " ;
                    if (ele.className == "num")
                    {
                        str = str + NumQP(ele.value)  ;    
                    }
                    else
                    {
                        str = str + "\"" +ele.value+ "\"" ;
                    }             
                str =  str + "} ";
                return str ;            
            }, 

        },         



        get: {   

            foreign: function ( f, obj ) {                   

                var campos = document.getElementById( f ).getElementsByTagName("input");    
                var idinput = 0;    

                for(var i=0; i< campos.length; i++) {                

                    var f = campos[i].dataset.foreign;

                    if ( f != undefined ) {                
                        if (obj.tipo == f.toString().toLocaleLowerCase()){    
                            idinput = campos[i].id;                
                        }                    
                    }                                
                }            
                return idinput;        
            },

        },


        radios: {   

            elementos : "",

            nombre: function ( nom ) {                   
                arasa.form.radios.elementos = document.getElementsByName(nom);
            },



            getvalor: function ( nom ) {    

                arasa.form.radios.nombre(nom);
                var ele = arasa.form.radios.elementos;
                var ret = "";

                for (i=0; i < ele.length; i++) {  
                  //console.log(ele[i]);
                    if (ele[i].checked){                    
                        ret = ele[i].value;
                    }
                }

                return ret;
            },




            disable: function ( nom, bool ) {    

                arasa.form.radios.nombre(nom);
                var ele = arasa.form.radios.elementos;

                for (i=0; i < ele.length; i++) {  
                  ele[i].disabled = bool;;
                }
            },


            databool: function (  nom, bool ) {    

                arasa.form.radios.nombre(nom);
                var ele = arasa.form.radios.elementos;

                for (i=0; i < ele.length; i++) {  

                    var myBool = Boolean(ele[i].value); 
                    if ( myBool === bool){
                        ele[i].checked = true;
                    }
                }


            },


            getdatabol: function (  nom ) {    

                arasa.form.radios.nombre(nom);
                var ele = arasa.form.radios.elementos;

                var str = "";        

                str =  str  + "\"" + nom + "\"" ;
                str =   str + ":";     

                str = str + arasa.form.radios.getvalor(nom) ;    

                return str;

            },

            getelejson: function ( ele, name ) {    


                var str = "";        

                str =  str  + "\"" + name + "\"" ;
                str =   str + ":";     

                
                if (ele.checked){                    
                    ret = "true";
                }
                else{
                    ret = "false";
                }

                return str + ret;

            },



        },





        checkbox: {   

            elementos : "",

            nombre: function ( nom ) {                   
                arasa.form.checkbox.elementos = document.getElementsByName(nom);
            },




            getvalor: function ( nom ) {    

                arasa.form.checkbox.nombre(nom);
                var ele = arasa.form.checkbox.elementos;
                var ret = "";


                for (i=0; i < ele.length; i++) {  


                    if (ele[i].checked){                    
                        ret = "true";
                    }
                    else{
                        ret = "false";
                    }
                }

                return ret;
            },




            databool: function (  ele, bool ) {    

                var myBool = Boolean(ele.value); 
                if ( myBool === bool){
                    ele.checked = true;
                }            
            },




            getdatabool: function (  ele ) {    
                var str = "";        
                str =  str  + "\"" + ele.name + "\"" ;
                str =   str + ":";     

                str = str + arasa.form.checkbox.getvalor(ele.name) ;  

                return str;

            },

        },


    },
    
    
    
    
    
    
    


    boton :
    {  
        class_c: "boton",
        class_a: "botonA",
        objeto: "",
        blabels: [],   
        funciones: null,

        ini: function( obj ) {
            arasa.boton.objeto = obj.tipo;        
            arasa.boton.class_a = "botonA";
        }  ,                                


        get_botton_html: function( etiqueta, indice ) {                                  

                strhtml = "";            
                strhtml += " <div class=\""+arasa.boton.class_c+"\"> ";
                strhtml += "    <a id=\"btn_"+arasa.boton.objeto+"_"+etiqueta.toString().toLowerCase() +"\"" ;
                strhtml += "        data-id=\""+indice +"\" ";
                strhtml += "        class=\""+arasa.boton.class_a +"\" ";
                strhtml += "        href=\"javascript:void(0);\">";    
                strhtml += "     "+ etiqueta;    
                strhtml += "    </a>";        
                strhtml += "   </div>";        

                return strhtml;
        },


        get_botton_base: function(  ) {                  
            strhtml = "";            
            arasa.boton.blabels.forEach(function (elemento, indice, array) {                                                        
                strhtml += arasa.boton.get_botton_html(elemento, indice);
            });                        
            strhtml =  " <div class=\"botones\">" + strhtml + "</div>";
            return strhtml;
        },    


        basicform: {    

            b_add: ['Guardar','Cancelar'],        
            b_reg: ['Nuevo','Modificar', 'Eliminar', 'Lista'],        
            b_edit: ['Editar','Cancelar'],        
            b_del: ['Eliminar','Cancelar'],              
            b_new: ['Nuevo'],      
            b_att: ['Agregar'],      
            b_cancel: ['Cancel'],     
            b_mrow: ['Modificar','Eliminar','Salir'],     
            b_newback: ['Nuevo','Atras'],     
            b_reg3: ['Nuevo','Eliminar', 'Lista'],  
            b_okcan: ['Aceptar','Cancelar'],        
            b_reg2nl: ['Nuevo', 'Lista'],  
            b_procan: ['Procesar','Cancelar'],        
            b_des: ['Descargar'],        
            b_new_prin_file: ['Nuevo', 'Imprimir', 'Excel'],  
            b_prin: [ 'Imprimir'],  
            b_pay_cancel: ['Pagar','Cancelar'],             
            b_newx: ['Nuevo', 'a .xlsx'],      
            b_reg_vista: ['Acciones', 'Atras'],        

            get_botton: function( eti ) {                  
                arasa.boton.blabels = eval("arasa.boton.basicform."+eti);
                return arasa.boton.get_botton_base();
            },                

            get_botton_add: function(  ) {                        
                arasa.boton.blabels = arasa.boton.basicform.b_add;
                return arasa.boton.get_botton_base();
            },        

            get_botton_reg: function(  ) {                        
                arasa.boton.blabels = arasa.boton.basicform.b_reg;
                return arasa.boton.get_botton_base();
            },     

            get_botton_edit: function(  ) {                        
                arasa.boton.blabels = arasa.boton.basicform.b_edit;
                return arasa.boton.get_botton_base();
            },            

            get_botton_del: function(  ) {                        
                arasa.boton.blabels = arasa.boton.basicform.b_del;
                return arasa.boton.get_botton_base();
            },     
            get_botton_new: function(  ) {                        
                arasa.boton.blabels = arasa.boton.basicform.b_new;
                return arasa.boton.get_botton_base();
            },          
            get_botton_att: function(  ) {                        
                arasa.boton.blabels = arasa.boton.basicform.b_att;
                return arasa.boton.get_botton_base();
            },          

            get_botton_cancel: function(  ) {                        
                arasa.boton.blabels = arasa.boton.basicform.b_cancel;
                return arasa.boton.get_botton_base();
            },                  
            get_botton_mrow: function(  ) {                        
                arasa.boton.blabels = arasa.boton.basicform.b_mrow;
                return arasa.boton.get_botton_base();
            },             

            get_botton_newback: function(  ) {                        
                arasa.boton.blabels = arasa.boton.basicform.b_newback;
                return arasa.boton.get_botton_base();
            },                     

            get_botton_okcan: function(  ) {                        
                arasa.boton.blabels = arasa.boton.basicform.b_okcan;
                return arasa.boton.get_botton_base();
            },           

            get_botton_reg2nl: function(  ) {                        
                arasa.boton.blabels = arasa.boton.basicform.b_reg2nl;
                return arasa.boton.get_botton_base();
            },            

            get_botton_procan: function(  ) {                        
                arasa.boton.blabels = arasa.boton.basicform.b_procan;
                return arasa.boton.get_botton_base();
            },            

            get_botton_des: function(  ) {                        
                arasa.boton.blabels = arasa.boton.basicform.b_des;
                return arasa.boton.get_botton_base();
            },            

            get_botton_new_prin_file: function(  ) {                        
                arasa.boton.blabels = arasa.boton.basicform.b_new_prin_file;
                return arasa.boton.get_botton_base();
            },            


            get_botton_prin: function(  ) {                        
                arasa.boton.blabels = arasa.boton.basicform.b_prin;
                return arasa.boton.get_botton_base();
            },            


            get_botton_pay_cancel: function(  ) {                        
                arasa.boton.blabels = arasa.boton.basicform.b_pay_cancel;
                return arasa.boton.get_botton_base();
            },     
            get_botton_newx: function(  ) {                        
                arasa.boton.blabels = arasa.boton.basicform.b_newx;
                return arasa.boton.get_botton_base();
            },              

            get_botton_reg_vista: function(  ) {                        
                arasa.boton.blabels = arasa.boton.basicform.b_reg_vista;
                return arasa.boton.get_botton_base();
            },  



        },


        evento: function( obj ) {



            /*
            alert( typeof  obj.funciones[0]);
            alert( obj.funciones[0]);
            */



            for (var i=0; i < arasa.boton.blabels.length; i++)
            {  

                var insta = document.getElementById(
                        ('btn_'+arasa.boton.objeto+'_'+arasa.boton.blabels[i]).toString().toLowerCase());            

                insta.onclick = function()
                {                                  

                    var valor_id = this.dataset.id;      
                    var fn = obj.funciones[valor_id];

                    reflex.funciones( obj, fn );    

                    /*
                    try { 
                        reflex.funciones( obj, fn );    
                    }
                    catch (e) {  
                        objetoclase.funciones( obj, fn );           
                    }                
                    */

                };       
            } 

        },




        accion: {

            modal_cerrar: function( obj ) {




                obj.break = false;
                modal.ventana.cerrar(obj.venactiva);                
            },

            modal_add: function( obj ) {
                modal.form.add(obj);     
            },        

            modal_del: function( obj ) {
               modal.form.del(obj);     
            },        

            modal_edi: function( obj ) {

                modal.form.edi(obj);     
            },             

            modal_update: function( obj ) {
                modal.form.update(obj);     
            },                     

            sublista: function( obj ) {       

                if (obj.break == false){
                    obj.sublista(obj);
                }

            },                        

            cabecera: function( obj ) {                             
                //reflex.acciones.button_reg(obj.cabecera);            
            },             


        } ,       


    },
 
 
    


    loader :
    {    
        dom: null,
        id:  "loader_id", 
       count: 0,

        inicio : function  ()
        {

            if ( arasa.loader.count == 0 ){



                var loade = document.createElement("div");
                loade.setAttribute("id", arasa.loader.id );
                loade.classList.add('loader');



                var loader_container = document.createElement("div");
                loader_container.classList.add('loader-container');

                var semicircle1 = document.createElement("div");
                semicircle1.classList.add('semicircle1');

                var semicircle2 = document.createElement("div");
                semicircle2.classList.add('semicircle2');

                loader_container.appendChild(semicircle1);
                loader_container.appendChild(semicircle2);

                loade.appendChild(loader_container);

                document.body.appendChild(loade);

                arasa.loader.dom = loade;


            }
            arasa.loader.count ++;

        },


        fin : function  ()
        {

            if (arasa.loader.count == 1) {
                document.body.removeChild( arasa.loader.dom );
            }
            arasa.loader.count --;

        },    



    },






};





