

var ajax_2 = 
{  
    
    server: '' ,  
    json: "",
    url: "",
    state: 0,    
    dataType: 'json',
    metodo: '',
    
    local:{
         token : ""
    },
    
    
    absolute: function() 
    {
        var pathname = window.location.pathname;
        pathname = pathname.substring(1,pathname.length);
        pathname = pathname.substring(0,pathname.indexOf("/"));
        return "/"+pathname ;            
    },    
    
    getserver: function() 
    {
        if (this.server == ""){
            return this.absolute()
        }
        else{
            return this.server;
        }           
    }, 
    
    
    
    
    
     
    promise: {               
        
        async : function ( metodoHtml ){    

            const promise = new Promise((resolve, reject) => {
                
                loader.inicio();

                var xhr =  new XMLHttpRequest();            
                var url = ajax.api;
                
                xhr.open( metodoHtml.toUpperCase(),   url,  true );      

                xhr.onreadystatechange = function () {
                    if (this.readyState == 4 ){
                        
                        loader.fin();
                        ajax.state = xhr.status;
                        resolve( xhr );
                        
                    }
                };
                xhr.onerror = function (e) {                    
                    reject(
                          xhr.status,
                          xhr.response   
                    );                 

                };                       

                //xhr.setRequestHeader("path", path );
                var type = "application/json";
                xhr.setRequestHeader('Content-Type', type);   
                //xhr.setRequestHeader("token", localStorage.getItem('token'));           

                xhr.send( ajax_2.json  );     
            });

            return promise;
        },        
        


    },

        
            
    
    
    
    
    headers: {    
        
        setRequest: function( ) {                                  
                ajax_2.req.setRequestHeader("token", localStorage.getItem('token'));
        },
        
        getResponse: function( ) {    
            
            ajax_2.local.token = ajax_2.req.getResponseHeader("token") ;            
            localStorage.setItem('token', ajax_2.local.token);     
            
            sessionStorage.setItem('total_registros',  ajax_2.req.getResponseHeader("total_registros"));
      
            
        },
        
        
        
        set: function( ) {                                  
                ajax_2.xhr.setRequestHeader("token", localStorage.getItem('token'));
        },
        
        
        
        get: function( ) {    
            
            ajax_2.local.token = ajax_2.xhr.getResponseHeader("token") ;            
            localStorage.setItem('token', ajax_2.local.token);     
            
            sessionStorage.setItem('total_registros',  ajax_2.xhr.getResponseHeader("total_registros"));
            
        },
  
           
        
        
    },


  
    private:{    

        json: function( data ) {                                           
            
            ajax_2.req =  new XMLHttpRequest();
            ajax_2.json = data ;                      
            ajax_2.req.open( ajax_2.metodo.toUpperCase(),   ajax_2.url,  false );              
            ajax_2.req.setRequestHeader('Content-Type', 'application/json');   
            
            ajax_2.headers.setRequest();
            ajax_2.req.send( ajax_2.json );                        
            ajax_2.headers.getResponse();
            
            ajax_2.state = ajax_2.req.status;      
            return  ajax_2.req.responseText;
        },
                

        
        jasper: function() {         
            
            ajax_2.req =  new XMLHttpRequest();             
            var url = html.url.absolute() +  "/TokenReport";                        
            ajax_2.req.open( "POST", url, false);              
            ajax_2.req.setRequestHeader('Content-Type', 'application/html;charset=UTF-8');
        
            ajax_2.headers.setRequest();              
            ajax_2.req.send();                    
            ajax_2.headers.getResponse();
        
            ajax_2.state = ajax_2.req.status;          
            if (ajax_2.state == 202){                
                window.open( ajax_2.url , '_blank');
            }
            //return ajax.req.responseText;               
        
        },                
                
             
        
    },
    
    
    
    
    public:{    
        
        html: function() {



            ajax_2.req =  new XMLHttpRequest(); 
            ajax_2.req.open( ajax_2.metodo.toUpperCase(), ajax_2.url, false);              
            ajax_2.req.setRequestHeader('Content-Type', 'text/html;charset=UTF-8');
        
console.log("deprecated")
console.log(ajax_2.url);
        
            ajax_2.req.send();  
            
            ajax_2.state = ajax_2.req.status;                    
            return ajax_2.req.responseText;
            
            
            
        },
     
        
    },
    
    
    
    

    async:{    
    
        get: function( ) {                                           


            const promise = new Promise((resolve, reject) => {

                    loader.inicio();
                    //obj.page = page;

                    var comp = window.location.pathname;                 
                    var path =  comp.replace( arasa.html.url.absolute() , "");

                    var xhr =  new XMLHttpRequest();      

                    var metodo = "GET";                         

                    xhr.open( metodo.toUpperCase(),   ajax.url,  true );      

                    xhr.onreadystatechange = function () {
                            if (this.readyState == 4 ){

                                loader.fin();                                
                                resolve( xhr );

                            }
                    };
                    xhr.onerror = function (e) {                    
                            reject(
                                xhr.status,
                                xhr.response   
                            );                 

                    };                       

                    xhr.setRequestHeader("path", path );
                    var type = "application/json";
                    xhr.setRequestHeader('Content-Type', type);   
                    xhr.setRequestHeader("token", localStorage.getItem('token'));           
                    xhr.send( ajax_2.json  );                       


            })

            return promise;
    
    
    
        },

    },

    
        
    
    
}






    function downloadFile(url, success) {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', url, true);
        //xhr.open('POST', url, true);
        
        xhr.contentType = 'application/pdf',
                
        xhr.responseType = "blob";
        xhr.onreadystatechange = function() {
            if (xhr.readyState == 4) {
                if (success) success(xhr.response);
            }
        };
        xhr.send(null);
    }           
        

                