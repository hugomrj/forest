
window.onload = function( dom ) {
    
    
    html.url.control( doMenu );
    
    function doMenu (){

        if (arasa.ajax.state == 200)
        {
            html.menu.mostrar();    

            var dom = 'arti_form';
            selector_lista_all ( dom );            
        }
    }
    
        
    
};



function selector_lista_all ( dom ){
   
    
    tabla.linea = "selector";    
    tabla.campos = ['selector', 'descripcion', 'nivel', 'superior', 'codigo', 'ord'];        
    tabla.tbody_id = "selectores-tb";    
           

    ajax_2.url = html.url.absolute() +'/app/basica' +'/'+'selector' + '/htmf/lista.html'; 
    ajax_2.metodo = "GET";            
    document.getElementById( dom ).innerHTML =  ajax_2.public.html();         

    ajax_2.url = html.url.absolute() +'/api/selectores/all' ;
    ajax_2.metodo = "GET";   
    
    tabla.json = ajax_2.private.json();  
    
    
    

    tabla.gene();
    tabla.id =  "selectores-tabla"  ;    
    tabla.lista_registro(  selector_registro  );
    
    selector_lista_formato (dom);
    
    
    var btn_nuevo = document.getElementById( "btn_nuevo" );
    btn_nuevo.addEventListener('click',
        function(event) {      
                        
            ajax_2.url = html.url.absolute() +'/app/basica/selector/htmf/form.html'; 
            ajax_2.metodo = "GET";  
            modal.ancho = 600;
            var obj = modal.ventana.mostrar("ind");       
            
            
            
            selector_modal_add( obj  );
            
        },
        false
    );     
    
    
    
    
}







function selector_modal_add ( obj ){
    
    
    document.getElementById( "botones_borrar" ).style.display = 'none';
        
    
    var selector_superior = document.getElementById( "selector_superior" );
    selector_superior.value = 0;
    //selector_superior.disabled=true;    
    
    var selector_id = document.getElementById( "selector_selector" );
    selector_id.value = 0;
    //selector_id.disabled=true;
    
    var selector_ord = document.getElementById( "selector_ord" );
    selector_ord.value = 0;
        
    var btn_selmod_nuevo = document.getElementById( "btn_selmod_aceptar" );
    btn_selmod_nuevo.addEventListener('click',
        function(event) {     
            
                        
            //    if ( main_form_validar() )
                if ( true )
                {
                    ajax_2.metodo = "post";                    
                    ajax_2.url = html.url.absolute()  +"/api/selectores";
                    
                    
                    arasa.form.name = "form_selector";
                    
                    var jsondata = ajax_2.private.json( arasa.form.datos.getjson() );  

                
                    if (ajax_2.state == 200) {
                        
                        
                        btn_selmod_cancelar.click();
                        selector_lista_all ( 'arti_form' );
                        arasa.msg.ok.mostrar("registro agregado");           
                        //objetoclase.form_id( dom, getJSONdataID(jsondata, objetoclase.nombre )  ) ;
                        
                        
                    }
                    else{
                        arasa.msg.error.mostrar("error de acceso");           
                    }                
                }
                
        }
    );    
    
    
    
    var btn_selmod_cancelar = document.getElementById( "btn_selmod_cancelar" );
    btn_selmod_cancelar.addEventListener('click',
        function(event) {     
            modal.ventana.cerrar(obj);
        }
    );    
    
    
};




function selector_modal_editdelete ( obj, linea ){
    
    ajax_2.url = html.url.absolute() +'/api/selectores/'+linea;    
    ajax_2.metodo = "GET";

    arasa.form.name = "form_selector" ;
    arasa.form.json = ajax_2.private.json();   
    
    arasa.form.campos = ['selector_id'];
    arasa.form.disabled(true);

    arasa.form.llenar();     


    var btn_selmod_aceptar = document.getElementById( "btn_selmod_aceptar" );
    btn_selmod_aceptar.innerHTML = "Editar";
    btn_selmod_aceptar.addEventListener('click',
        function(event) {     
                
            //if ( main_form_validar() )
            if ( true )
            {
                var id = document.getElementById('selector_selector').value;
                ajax_2.metodo = "put";

                ajax_2.url = html.url.absolute() +"/api/selectores/"+id;                  
                
                arasa.form.name = "form_selector";


                var jsondata = ajax_2.private.json( arasa.form.datos.getjson() );                  
                

                if (ajax_2.state == 200){

                    btn_selmod_cancelar.click();
                    selector_lista_all ( 'arti_form' );                        
                    arasa.msg.ok.mostrar("registro editado");                                   

                }
                else{
                    arasa.msg.error.mostrar("error de acceso");           
                } 
            }
        }
    ); 


    var btn_selmod_borrarr = document.getElementById( "btn_selmod_borrarr" );
    btn_selmod_borrarr.addEventListener('click',
        function(event) {     
                
            var id = document.getElementById('selector_selector').value;

            ajax_2.metodo = "delete";            
            ajax_2.url = arasa.html.url.absolute() +"/api/selectores/"+id;        
            
            ajax_2.private.json( null );  

            if (ajax_2.state == 200){

                btn_selmod_cancelar.click();
                selector_lista_all ( 'arti_form' );                        
                arasa.msg.ok.mostrar("registro editado");                   
            }
            else{
                arasa.msg.error.mostrar("error");           
            }                 
                
        }
    );    




    var btn_selmod_cancelar = document.getElementById( "btn_selmod_cancelar" );
    btn_selmod_cancelar.addEventListener('click',
        function(event) {     
                modal.ventana.cerrar(obj);
        }
    );    
    

};





function selector_registro ( linea ){
    
    
        ajax_2.url = html.url.absolute() +'/app/basica/selector/htmf/form.html'; 
        ajax_2.metodo = "GET";       

        modal.ancho = 600;
        var obj = modal.ventana.mostrar("ide");   
        selector_modal_editdelete( obj, linea  );    
        
        // tabuladores
        var obj = new Selector();
        obj.tabuladores();

};




function selector_lista_formato ( tabla ){

    var table = document.getElementById( tabla ).getElementsByTagName('tbody')[0];
    var rows = table.getElementsByTagName('tr');

    for (var i=0 ; i < rows.length; i++)
    {
        cell = table.rows[i].cells[1] ;                                  
        //cell.innerHTML = fmtNum(cell.innerHTML);  
        cell.innerHTML = '&nbsp'.repeat(table.rows[i].cells[2].innerHTML * 5) + cell.innerHTML;            
    }



};







function main_lista ( dom  ){
};


function main_form_validar( ){
    return true;
};


function main_form_edit( ){    
};


function busquedalista( ){    
};




                





window.onresize = function() {
    document.body.style.minheight  = "100%" ;  
    var nodeList = document.querySelectorAll('.capaoscura');
    for (var i = 0; i < nodeList.length; ++i) {
        document.getElementById(nodeList[i].id).style.height = document.body.scrollHeight.toString() + "px";                
    }
};








