

function UsuarioRol(){
        
    this.tipo = "usuariorol";   
    this.recurso = "usuariorol";   
    this.value = 0;

   this.carpeta=  "/app/basica";   


    this.campoid=  'id';
    this.tablacampos = ['id', 'usuario.usuario', 'usuario.cuenta',  'rol.rol', 'rol.nombre' ];    
    this.tablacamposoculto = [];    
    
    this.formcampooculto = [];     
        
    this.titulosin = "";
    this.tituloplu = "" ; 
    
    this.botones_lista = "acciones-lista";
    this.botones_form = "usuariorol-acciones";    
    
        
    this.funciones = null;  
    this.venactiva = null;

    this.cabecera= "";    
    this.foreign = [ new Usuario(), new Rol() ] ;
    
    this.break = false;    
};




UsuarioRol.prototype.sublista = function( objeto ) {                
           

        tabla.setObjeto(objeto);
        //coleccion.sublista( obj );     
                               
        
        var tabsbody = document.getElementById('tabsbody');                    
        
        ajax_2.url =  html.url.absolute() + objeto.carpeta +'/'+objeto.tipo + '/htmf/lista.html';   

        
        ajax_2.metodo = "GET";            
        tabsbody.innerHTML =  ajax_2.public.html();     

        arasa.boton.objeto = objeto.tipo;
        var acc = document.getElementById( objeto.botones_lista );         
        acc.innerHTML = arasa.boton.basicform.get_botton_att();         

        var btn_usuariorol_agregar = document.getElementById( "btn_usuariorol_agregar" );  
        btn_usuariorol_agregar.addEventListener('click', function() {         
            objeto.modal_new(  objeto  ) ;
        });    


        // mostrar lista  

        ajax_2.url = reflex.getApiRecursoMTM( objeto, 1, null );      
        ajax_2.metodo = "GET";        
        arasa.tabla.json = ajax_2.private.json();             
        

        arasa.tabla.ini(objeto);               
        arasa.tabla.gene();        
        
        
        
        // aca me falta un evento para modifioca el registro o al menos para borrar
         
        objeto.sublista_reg = function( objeto, id ) {
            
            objeto.value = id;

            
            reflex.ini( objeto );    
            ajax_2.url =  html.url.absolute() + objeto.carpeta  +'/'+objeto.tipo + '/htmf/form.html';                        
            ajax_2.metodo = "GET";  


            
            objeto.venactiva = modal.ventana.mostrar( objeto.tipo + "reg");                  
            var dom = objeto.venactiva.firstChild.id;                 
            
            // titulo            
            var primer = document.getElementById( dom ).firstChild;
            var htitulo = document.createElement("h4");
            htitulo.innerHTML = "Usuario";
            document.getElementById( dom ).insertBefore(htitulo,  primer);     


            // ocultar campos            
            document.getElementById('f-line-rol').style.display = "none"  ;
            document.getElementById('f-line-usuario').style.display = "none"  ;                        
            document.getElementById('usuariorol_id').disabled = true;



            // solo eliminar y cancelar
            var bo = document.getElementById( "usuariorol-acciones" );     
            boton.objeto = objeto.tipo;              
            bo.innerHTML = boton.basicform.get_botton_del() ;             
            


            ajax_2.url = html.url.absolute() +'/api/usuariorol/'+objeto.value;    
            ajax_2.metodo = "GET";

            arasa.form.name = "form_form_usuariorol";
            arasa.form.json = ajax_2.private.json();   
            var ojson = JSON.parse(ajax_2.private.json());

            document.getElementById("usuariorol_id").value = ojson.id;            
            

            //form.llenar();     




            var btn_usuariorol_cancelar = document.getElementById('btn_usuariorol_cancelar');
            btn_usuariorol_cancelar.onclick = function(event) {                     
                modal.ventana.cerrar(objeto.venactiva);  
            }
        
                          


            var btn_usuariorol_eliminar = document.getElementById('btn_usuariorol_eliminar');
            btn_usuariorol_eliminar.onclick = function(event) {                     
             

                var id = objeto.value;

                arasa.ajax.metodo = "delete";            
                arasa.ajax.url = arasa.html.url.absolute() +"/api/usuariorol/"+id;           
                arasa.ajax.private.json( null );
    
                if (arasa.ajax.state == 200){
                    arasa.arasa.msg.ok.mostrar("registro eliminado");      
                }
                else{
                    if (ajax.state == 502 ||  ajax.state == 500  ){                                                 
                        arasa.msg.error.mostrar(ajax.req.responseText);                            
                    }
                    else{
                        arasa.msg.error.mostrar("error de acceso");           
                    }                                        
                }   

                objeto.sublista(objeto);
                modal.ventana.cerrar(objeto.venactiva);  
            }



           




        }        
        
        
        

        // posiblemente borrar
        //tabla.lista_registro( objeto,  coleccion.sublista_reg );           

        tabla.lista_registro( objeto,  objeto.sublista_reg );                   
        
        
                
        tabla.oculto = objeto.tablacamposoculto;        
        tabla.ocultar();   


/*
        objeto.funciones = [ objeto.modal_new ] ;
        boton.evento(objeto);
*/



/*
        ajax.url = reflex.getApiRecursoMTM( objeto, 1, null );      
        
       
        ajax.metodo = "GET";        
        tabla.json = ajax.private.json();             
        
        
        tabla.ini(objeto);               
        tabla.gene();        
        
        tabla.lista_registro( objeto,  coleccion.sublista_reg );           
                
        tabla.oculto = objeto.tablacamposoculto;        
        tabla.ocultar();                
   */         
        
        
        
        

};





UsuarioRol.prototype.modal_new = function(  obj  ) {    


//      modal.form.new( obj );  
     
    
    ajax_2.url = html.url.absolute()  +  '/app/basica/usuariorol/htmf/form.html';                
    ajax_2.metodo = "GET";       
        
    modal.ancho = 800;
    obj.venactiva = modal.ventana.mostrar( obj.tipo + "add");                  
    var dom = obj.venactiva.firstChild.id;       
    
    obj.titulosin = "Usuario";
    reflex.setTitulosObjeto(obj);
    reflex.getTitulosin(dom, "h5");      
    

    // ocultar campo ralacionado 
    document.getElementById('f-line-rol').style.display = "none"  ;


    // viculcar cabecere y ocultar partes
    document.getElementById('usuariorol_id').value = 0  ;
    document.getElementById('usuariorol_rol_rol').value = document.getElementById('rol_rol').value  ;


    // botones de busqueda
    /*
    obj.foreign = [ new Usuario(), new Rol() ] ;        
    reflex.ui.foreign_more(obj);    
    */
    var ico_more_usuario = document.getElementById('ico-more-usuario');
    ico_more_usuario.onclick = function(  )
    {  
 
        var usuario =new Usuario();        
        usuario.acctionresul = function( id ) {
            document.getElementById('usuariorol_usuario_usuario').value = id;
        }
        busqueda.modal.objeto(usuario);

    };    
    
    

    
    
    if (!(obj.botones_form === undefined)) {    
        var bo = document.getElementById( obj.botones_form );                       
    }
    else {
        var bo = document.getElementById( obj.tipo + "-acciones" );                       
    }
    boton.objeto = obj.tipo;
    bo.innerHTML = boton.basicform.get_botton_add() ;






    var btn_usuariorol_guardar = document.getElementById('btn_usuariorol_guardar');
    btn_usuariorol_guardar.onclick = function(event) {     
        
        
        modal.form.add(obj);    
        
        obj.sublista(obj);
        /*
                 boton.accion.modal_add ,  
                 boton.accion.sublista        
        */
    }




    var btn_usuariorol_cancelar = document.getElementById('btn_usuariorol_cancelar');
    btn_usuariorol_cancelar.onclick = function(event) {     
        obj.break = false;
        modal.ventana.cerrar(obj.venactiva);  
    }

        

 
    
    
    
    


/*
        obj.funciones = [  
             [
                 boton.accion.modal_add ,  
                 boton.accion.sublista
            ],
             [
                 boton.accion.modal_cerrar ,  
                 boton.accion.sublista
            ]
        ];  
        boton.evento( obj );      
*/
    
    
    
    
    
/*
    obj.foreign = [ new Usuario(), new Rol() ] ;
        
    reflex.ui.foreign_more(obj);
    
    
    // posiblemente borrar 
    //reflex.ui.foreign_plus(obj);
    reflex.ui.foreign_decrip(obj);

    modal.form.pasarcab(obj);
    modal.form.ocultarcab(obj);
    
    
    var caid = document.getElementById( "usuariorol_id");
    caid.value = 0;    
    caid.disabled=true;
  */  
    
    
};






UsuarioRol.prototype.form_validar = function( objeto ) {    

    var usuariorol_id = document.getElementById('usuariorol_id');    
    if (usuariorol_id.value == "")         
    {
        arasa.msg.error.mostrar("error campo vacio");                   
        return false;
    }        
    
    var usuariorol_usuusu = document.getElementById('usuariorol_usuario_usuario');    
    if (usuariorol_usuusu.value == "")         
    {
        arasa.msg.error.mostrar("error campo usuario");                   
        return false;
    }  
    
    return true;
}







