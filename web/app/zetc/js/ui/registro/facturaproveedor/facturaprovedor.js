let pjson = "";

function FacturaProveedor(){
        
    this.tipo = this.constructor.name.toLowerCase();    
    this.campoid =  "id";    
    this.json = ""; 
    this.tablacampos = ['id', 'fecha' , 'obra.descripcion', 'numero_factura',
                        'timbrado', 'tipo_compra.descripcion', 'proveedor.descripcion',
                        'estado_pago.descripcion', 'total_factura', 
                        'tipo_cheque.descripcion', 'dias_diferidos'  ];
    this.recurso = "facturaproveedor";
        
    this.page = 1;    
    
}


let omateriales;




FacturaProveedor.prototype.lista = function( page ) {        
    
    var obj = this;           
    
    arasa.loader.inicio();    
        
    // tal vez aca llame al la clase            
    fetch('htmf/lista.html')
        .then(response => response.text())
        .then(html => {
            

            document.getElementById( arasa.dom.main ).innerHTML = html;
        

            arasa.boton.objeto = obj.tipo;
            document.getElementById( obj.tipo  + '_acciones_lista' ).innerHTML 
            =  arasa.boton.basicform.get_botton_att();       



            var btn_agregar = document.getElementById('btn_'+ obj.tipo +'_agregar');              
            btn_agregar.onclick = function(  ) {  
                obj.nuevo();    
            }  


            arasa.ajax.api = arasa.html.url.absolute() +  "/api/"+obj.recurso+"?page="+page;
            arasa.ajax.json = null;
            
            

            arasa.ajax.promise.async("get").then(result => {
                obj.json = result.responseText;
                        
                
                var ojson = JSON.parse( obj.json ) ; 
                arasa.tabla.json = JSON.stringify(ojson['datos']);     

                arasa.tabla.ini(obj);                
                arasa.tabla.gene();  
                
                //boo.tabla.lista_registro(obj, reflex.form_id_promise ); 
                arasa.tabla.id =  obj.tipo+"_tabla";
                arasa.tabla.lista_registro(obj); 
                
                arasa.vista.paginacion_html(obj,  JSON.stringify(ojson['paginacion']), page  );   

            });
            
        })
        /*
        .catch(error => {
            console.error('Error al cargar la página:', error);
        })
        */
        .finally(() => {               
            arasa.loader.fin();
        });    

};








FacturaProveedor.prototype.nuevo = function( ) {                
    
    var obj = this;        
    arasa.loader.inicio();
                        
        
    arasa.ajax.api = arasa.html.url.absolute()  +  "/api/materialmanoobra/1/all"        
    arasa.ajax.promise.async("get").then(result => {

        omateriales = JSON.parse( result.responseText ) ;    
                

        fetch('htmf/form_registro.html')        
            .then(response => response.text())
            .then(html => {            
                document.getElementById( arasa.dom.main ).innerHTML = html;      
                
                return html;
            })
            .then(html => {       


                arasa.loader.inicio();
                fetch('htmf/form_caberera.html')        
                    .then(response => response.text())
                    .then(html => {            
                        document.getElementById( "form_caberera" ).innerHTML = html;      
                        obj.form_ini();
                        
                        
                        return html;
                    })
                    .then(html => {                 
                        
                        var anio = document.getElementById('anio');
                        anio.disabled = true;                        

                        obj.carga_combos_new();

                        // antes de poner detalle tengo que agregar la cabecera de la cuadricula                    
                        obj.generar_linea_detalle(1);
                        obj.eventos_linea(1);

                        return html;
                    })        
                    .then(html => {                 

                        obj.agregar_registro_detalles();

                        obj.botones_accion_add(); 
                        
                        return html;
                    })        


                    .catch(error => {
                        console.error(error);
                    })                                
                    .finally(() => {                                   
                        arasa.loader.fin();
                    });             

                return html;
            })
            .finally(() => {               
                arasa.loader.fin();
            }); 

        
    });        
        
        

};









FacturaProveedor.prototype.form_ini = function() {    
    

    var dias_diferidos = document.getElementById('dias_diferidos');                
    dias_diferidos.onblur  = function() {                  
         dias_diferidos.value = fmtNum(dias_diferidos.value);      
    };      
    dias_diferidos.onblur();    
   

    
};





FacturaProveedor.prototype.carga_combos_new = function( dom ) {                  
    /*
    var oficio = new Oficio();
    oficio.combobox("contratista_oficio", 1);
 */


    var obra = new ObraRealizada();
    obra.combobox("facturaproveedor_obra_realizada", 1);

    
    
    var tipofactura = document.getElementById( "facturaproveedor_tipo_factura" );
    var opt = document.createElement('option');            
    opt.value = 1;
    opt.innerHTML = 'Proveedor';                        
    tipofactura.appendChild(opt);          
    tipofactura.disabled = true;
    
    
    
    var tipo_compra = new TipoCompra();
    tipo_compra.combobox("facturaproveedor_tipo_compra", 1);
    
    
    
    var proveedor = new Proveedor();
    proveedor.combobox("facturaproveedor_proveedor", 1);
    
    
    /*
    var estado_pago = document.getElementById( "estado_pago" );
    var opt = document.createElement('option');            
    opt.value = 1;
    opt.innerHTML = 'Pendiente';                        
    estado_pago.appendChild(opt);          
    estado_pago.disabled = true;
     */  
    
    var estado_pago = new EstadoPago();
    estado_pago.combobox("estado_pago", 1);
    
    
    
    var tipo_cheque = new TipoCheque();
    tipo_cheque.combobox("facturaproveedor_tipo_cheque", 1);


 };
 








FacturaProveedor.prototype.botones_accion_add = function( ) {    
    var obj = this;
    


    arasa.boton.objeto = obj.tipo;
    document.getElementById( obj.tipo + '_acciones' ).innerHTML 
    =  arasa.boton.basicform.get_botton_add();       
    

    

    var btn_cancelar = document.getElementById('btn_'+obj.tipo+'_cancelar');
    btn_cancelar.className = 'botonB';
    btn_cancelar.onclick =  function() {            
        obj.lista(1);        
    }; 




    var btn_guardar = document.getElementById('btn_' + obj.tipo + '_guardar');
    btn_guardar.addEventListener('click', function() {
        
        //let obj = new Cliente();                
        if ( obj.validar()){      
                        
            
            arasa.form.name = "form_facturaproveedor";


            arasa.ajax.api = arasa.html.url.absolute() + "/api/facturaproveedor/facturar";
            arasa.ajax.json = obj.getjson() ;
            
            
            arasa.ajax.promise.async("post").then(result => {                
                json = result.responseText;            

                if (result.status == 200){                               
                            
                    var codigo = result.responseText;          
                    
                    obj.tabla_registro(codigo);
                            
                    /*                                        
                    obj.tabla_registro(codigo_venta);
                    */                    
                    arasa.msg.ok.mostrar("Registro agregado"); 
                }
                else {                    
                    arasa.msg.error.mostrar(result.responseText); 
                }                
                
            });  
            
            
        }      
    });    
    
    

}





 FacturaProveedor.prototype.generar_linea_detalle = function( numeroLinea ) {   
 
    
    
    // Crear el elemento <tr>
    var row = document.createElement("tr");

    // Establecer el atributo data-linea_id
    row.setAttribute("data-linea_id", numeroLinea);

    // Crear y añadir las celdas al elemento <tr>
    var cell1 = document.createElement("td");        
    var borrar_linea = document.createElement("a");
    
    borrar_linea.id = "borrar_linea_"+numeroLinea;
    borrar_linea.textContent = "X";
    borrar_linea.style.cursor = "pointer";
    borrar_linea.style.color = "red";
    borrar_linea.style.fontWeight = "bold";
    cell1.appendChild(borrar_linea);    
    row.appendChild(cell1);


    // select 
    var cell2 = document.createElement("td");
    
    const select1 = document.createElement('select');
    select1.id = 'material_' + numeroLinea;
    select1.name = 'material_' + numeroLinea;
    cell2.appendChild(select1);
    
    row.appendChild(cell2);



    // unidad medida
    var cell3 = document.createElement("td");
    cell3.id = "unidadmedida_" + numeroLinea ;
    cell3.textContent = "";
    cell3.style.textAlign = "center";
    row.appendChild(cell3);




    // cantidad
    var cell4 = document.createElement("td");
    var input_cantidad = document.createElement("input");    
    input_cantidad.id = "cantidad_" + numeroLinea ;    
    input_cantidad.type = "text"; 
    input_cantidad.value = "0"; 
    input_cantidad.style.border = "none";
    input_cantidad.style.outline = "none";
    input_cantidad.style.textAlign = "right";
    input_cantidad.style.width = "100%";
    cell4.appendChild(input_cantidad);
    row.appendChild(cell4);    






    // precio unitario
    var cell5 = document.createElement("td");
    var input_precio = document.createElement("input");
    input_precio.id = "precio_unitario_" + numeroLinea ;    
    input_precio.type = "text"; 
    input_precio.value = "0"; 
    input_precio.style.border = "none";
    input_precio.style.outline = "none";
    input_precio.style.textAlign = "right";
    input_precio.style.width = "100%";

    cell5.appendChild(input_precio);
    row.appendChild(cell5);    
    
    
    // sub total linea
    var cell6 = document.createElement("td");
    var input_subtotal = document.createElement("input");
    input_subtotal.id = "subtotal_" + numeroLinea ;    
    input_subtotal.type = "text"; 
    input_subtotal.value = "0"; 
    input_subtotal.style.border = "none";
    input_subtotal.style.outline = "none";
    input_subtotal.style.textAlign = "right";
    input_subtotal.style.width = "100%";
    input_subtotal.setAttribute("readonly", true);    
    input_subtotal.setAttribute("data-columna", "subtotal");    
    cell6.appendChild(input_subtotal);    
    row.appendChild(cell6);
    
         
    
    
    // losffocus
    
    input_cantidad.addEventListener("blur", function() {
        input_cantidad.value = fmtNum(input_cantidad.value);    
        
        input_subtotal.value = NumQP(input_cantidad.value)  *  NumQP(input_precio.value)         
        input_subtotal.value = fmtNum(input_subtotal.value);            
        new FacturaProveedor().suma_total();
    });    
    
    
    input_precio.addEventListener("blur", function() {
        input_precio.value = fmtNum(input_precio.value);    
        
        input_subtotal.value = NumQP(input_cantidad.value)  *  NumQP(input_precio.value)         
        input_subtotal.value = fmtNum(input_subtotal.value);    
        new FacturaProveedor().suma_total();
    });    
    
        
    document.getElementById('cuadricula_detalles').appendChild(row);    
    
 };
 
 
 
 
 
 

FacturaProveedor.prototype.agregar_registro_detalles = function(  ) {  
    
    var obj = this;

    let indice = 1;    
    

    var btn_agregar_linea = document.getElementById( "btn_agregar_linea" );

    var link = document.createElement("a");
    // Establecer atributos al elemento <a>
    link.id = "boton_add";    
    link.className = "botonC";
    link.href = "javascript:void(0);";
    link.textContent = "agregar linea";

    // Agregar el elemento <a> al cuerpo del documento
    btn_agregar_linea.appendChild(link);

    
    link.onclick = function()
    {   
        
        indice++;
        obj.generar_linea_detalle(indice);
        obj.eventos_linea(indice);

    }    


}






FacturaProveedor.prototype.eventos_linea = function( numeroLinea ) {   

    let ojson ;

    // borrar linea
    const botonBorrar = document.getElementById('borrar_linea_' + numeroLinea);
    if (botonBorrar) {
        botonBorrar.addEventListener('click', function() {            
            
            var td = botonBorrar.parentNode; // Obtiene el padre <td> del enlace
            var tr = td.parentNode; // Obtiene el padre <tr> del <td>            
            tr.remove(); // Elimina el <tr> del DOM            
            new FacturaProveedor().suma_total();
                        
                     
        });
    }




    // select choises.js
    var material_producto = document.getElementById( "material_" + numeroLinea );
    material_producto.innerHTML = '';
    for( x=0; x < omateriales.length; x++ ) {
                  
        var opt = document.createElement('option');            
        opt.value = omateriales[x]['id'];
        opt.innerHTML = omateriales[x]['descripcion'];                        
        material_producto.appendChild(opt);                     

    }    


          
 
    // Inicializa Choices.js en el elemento <select>
    var choices_producto = new Choices(material_producto, {
        position: 'bottom',   
        loadingText: 'buscando..',
        noResultsText: 'no se encontraron resultados',
        noChoicesText: 'escriba para buscar',
        searchResultLimit: 10
    });        




    choices_producto.passedElement.element.addEventListener(
        'choice',
        function(event) {          

            var opcionSeleccionada = event.detail.choice;           

            arasa.ajax.api = arasa.html.url.absolute() +  "/api/materialmanoobra/"+opcionSeleccionada.value;            
            arasa.ajax.promise.async("get").then(result => {
                                
                var objeto = JSON.parse(result.responseText);                
                var unidadmedida = document.getElementById( "unidadmedida_" + numeroLinea );
                unidadmedida.innerHTML = objeto.unidad_medida.descripcion ; 
                
            });





/*

            productos = ojson;
            const productoElegido = productos.find(producto => producto.producto === opcionSeleccionada.value);            

            var precio_variable = productoElegido.precio_variable;

            // cargar los calores a la cadricula
            var preciounitario = document.getElementById( "preciounitario_" + numeroLinea );
            preciounitario.value = productoElegido.precio_unitario;

            
            // si es precio variable tiene que desbloquearse el campo
            if (precio_variable == true) {
                preciounitario.disabled = false;
            }
            else {
                preciounitario.disabled = true;
            }


            // iva 
            document.getElementById('linea_'+numeroLinea)
                    .setAttribute('data-iva',  productoElegido.tasa_iva.tasa_iva );


            // unidada medida
            var unidadmedida = document.getElementById( "unidadmedida_" + numeroLinea );
            unidadmedida.value = productoElegido.unidad_medida.unimed;

            
            var cantidad = document.getElementById( "cantidad_" + numeroLinea );
            //cantidad.blur();
            cantidad.focus();
            cantidad.select();   

*/

        },
        false,
    );      





    var comboinput = material_producto.parentNode.children[1];
    comboinput.textContent = '';
    comboinput.setAttribute('data-id', '0');
    comboinput.setAttribute('data-value', '0');





}











FacturaProveedor.prototype.suma_total = function(  ) {  



    // Obtener el contenedor
    const cuadriculaDetalles = document.getElementById('cuadricula_detalles');

    var suma_subtotal = 0;
    const elementosConIva10 = cuadriculaDetalles.querySelectorAll('[data-columna="subtotal"]');
    elementosConIva10.forEach((elemento) => {
        // Obtener el contenido del elemento y convertirlo a número
        const valorElemento = parseFloat( NumQP(elemento.value));    
        // Verificar si la conversión fue exitosa y agregar al total        
        if (!isNaN(valorElemento)) {
            suma_subtotal += valorElemento;
        }
    });
    document.getElementById('total_general').innerHTML = fmtNum(suma_subtotal);       
    

    

}
















FacturaProveedor.prototype.validar = function() {   
    

    var fecha = document.getElementById("facturaproveedor_fecha");    
    if (fecha.value === "") {
        
        arasa.msg.error.mostrar("La fecha esta vacia");     
        fecha.focus();
        fecha.select();        
        return false;
    } 
    
    
    // obra realizada vacia
    var selectElement = document.getElementById('facturaproveedor_obra_realizada');    
    var valorSeleccionado = selectElement.value;
    var textoSeleccionado = selectElement.options[selectElement.selectedIndex].text;
    if (textoSeleccionado === "") {
        
        arasa.msg.error.mostrar("Falta seleccionar la obra");     
        selectElement.focus();        
        return false;
    } 



    var numero_factura = document.getElementById("facturaproveedor_numero_factura");    
    if (numero_factura.value === "") {
        
        arasa.msg.error.mostrar("El numero de factura esta vacio");     
        numero_factura.focus();
        numero_factura.select();        
        return false;
    } 



    var timbrado = document.getElementById("facturaproveedor_timbrado");    
    if (timbrado.value === "") {
        
        arasa.msg.error.mostrar("El timbrado esta vacio");     
        timbrado.focus();
        timbrado.select();        
        return false;
    } 



    var ruc = document.getElementById("ruc");    
    if (ruc.value === "") {
        
        arasa.msg.error.mostrar("El Ruc esta vacio");     
        ruc.focus();
        ruc.select();        
        return false;
    } 



    // detalle de factura    
    var totalGeneralElement = document.getElementById('total_general');
    var contenidoHTML = parseInt(totalGeneralElement.innerHTML);      
    if (contenidoHTML === 0) {
        arasa.msg.error.mostrar("La factura esta vacia");             
        return false;
    } 



    // Obtener la tabla por su ID
    var encontradoCero = false;
    var tablaFacturas = document.getElementById('facturaproveedor_tabla');    
    var inputs = tablaFacturas.querySelectorAll('input[data-columna="subtotal"]');    
    inputs.forEach(function(input) {
        //console.log(input.value);
        stvalor = NumQP(input.value)        
        if (parseInt(stvalor) === 0){                
            encontradoCero = true;
            return false;    
        }
    });
    if (encontradoCero) {
        arasa.msg.error.mostrar("Existen registros del subtotal con valor 0"); 
        return false;
    }




    return true;
    
};







FacturaProveedor.prototype.validar_edit = function() {   
    


    return true;
    
};











FacturaProveedor.prototype.getjson = function( ) {  

    ret = "";


    var fecha = document.getElementById('facturaproveedor_fecha');
    var obra = document.getElementById('facturaproveedor_obra_realizada');    
    var numero_factura = document.getElementById('facturaproveedor_numero_factura');    
    var timbrado = document.getElementById('facturaproveedor_timbrado');    
    var tipo_compra = document.getElementById('facturaproveedor_tipo_compra');    
    var proveedor = document.getElementById('facturaproveedor_proveedor');    
    var ruc = document.getElementById('ruc');    
    var fecha_pago = document.getElementById('fecha_pago');    
    var numero_cheque = document.getElementById('numero_cheque');    
    var estado_pago = document.getElementById('estado_pago');    
    
    var tipo_cheque = document.getElementById('facturaproveedor_tipo_cheque');    
    var dias_diferidos = document.getElementById('dias_diferidos');    
    

    // Crear el objeto JSON
    var formData = {
        fecha: fecha.value,
        obra: obra.value,
        numero_factura: numero_factura.value,
        timbrado: timbrado.value,
        tipo_compra : tipo_compra.value,
        proveedor : proveedor.value,
        ruc : ruc.value,
        fecha_pago : fecha_pago.value,
        numero_cheque : numero_cheque.value,
        estado_pago : estado_pago.value,
        tipo_cheque : tipo_cheque.value,
        dias_diferidos : Number( NumQP( dias_diferidos.value ))
    };
  
   

    // detalle    
    var detalle = [];    
    var cuadriculaDetalles = document.getElementById('cuadricula_detalles');
    var lineas = cuadriculaDetalles.children;

    // Iterar sobre los elementos <div> en el primer nivel de anidamiento
    for (var i = 0; i < lineas.length; i++) {

        var lineaHTML = lineas[i];
        var nroLinea = lineaHTML.dataset.linea_id;


        // material
        var selectElement =  document.getElementById('material_' +nroLinea );
        var valorSelect = selectElement.value;

        var cantidad =  document.getElementById('cantidad_' +nroLinea );
        var precio_unitario =  document.getElementById('precio_unitario_' +nroLinea );
        var subtotal =  document.getElementById('subtotal_' +nroLinea );
        
        
        
        // Crear un objeto JSON para cada línea
        var lineaJSON = {
            
          material : Number( NumQP( valorSelect )), 
          cantidad : Number( NumQP( cantidad.value )),         
          precio_unitario : Number( NumQP( precio_unitario.value )),    
          subtotal : Number( NumQP( subtotal.value ))   
          
        };
        
        // Agregar el objeto JSON al array de líneas
        detalle.push(lineaJSON);
    }
    
    formData.detalle = detalle ;      
    

    ret =  JSON.stringify(formData);
    return ret;
}








FacturaProveedor.prototype.getjson_edit = function( ) {  

    ret = "";


  
    var fecha_pago = document.getElementById('fecha_pago');    
    var numero_cheque = document.getElementById('numero_cheque');    
    var estado_pago = document.getElementById('estado_pago');    
    
    var tipo_cheque = document.getElementById('facturaproveedor_tipo_cheque');    
    var dias_diferidos = document.getElementById('dias_diferidos');    
    

    // Crear el objeto JSON
    var formData = {
        fecha_pago : fecha_pago.value,
        numero_cheque : numero_cheque.value,
        estado_pago : estado_pago.value,
        tipo_cheque : tipo_cheque.value,
        dias_diferidos : Number( NumQP( dias_diferidos.value ))
    };
     
    ret =  JSON.stringify(formData);
    return ret;
}






FacturaProveedor.prototype.form_disabled = function( bool ) {  
    
    arasa.form.disabled(bool);  
    //document.getElementById("facturaproveedor_obra_realizada").disabled =  !bool;
    
}






FacturaProveedor.prototype.tabla_registro = function( id ) {    
    var obj = this;

    arasa.loader.inicio();    


    fetch('htmf/form_registro.html')        
        .then(response => response.text())
        .then(html => {            
            document.getElementById( arasa.dom.main ).innerHTML = html;      
            
            return html;
        })
        .then(html => {    

            arasa.loader.inicio();
            fetch('htmf/form_caberera.html')        
                .then(response => response.text())
                .then(html => {            
                    document.getElementById( "form_caberera" ).innerHTML = html;      
                    obj.form_ini();
                    //obj.botones_accion_registro(id);   
                    return html;
                })
                .then(html => {                 
                    
                    arasa.ajax.api = arasa.html.url.absolute() +  "/api/"+obj.recurso+"/"+id;            
                    arasa.ajax.promise.async("get").then(result => {
                        
                        obj.json = result.responseText;                                
                        pjson = result.responseText;                                


                        // cargar form
                        arasa.form.name = "form_facturaproveedor";
                        arasa.form.json = obj.json;   
                        //arasa.form.disabled(false); 
                        arasa.form.llenar();                
                        obj.cargar_formulario(result.responseText);
                        //arasa.form.llenar_cbx(obj);  


                        obj.form_disabled(false);
                        obj.botones_accion_registro(id); 
                        
                    });                    
                    
                    return html;
                    
                })        
                .finally(() => {                                   
                    arasa.loader.fin();
                });             
            return html;
        })
        .finally(() => {
            arasa.loader.fin();            
        });        

   
    
}






FacturaProveedor.prototype.botones_accion_registro = function( id) {  

    var obj = this;
    obj.id = id;
        
    var divBotonera = document.getElementById('botonera');
    /*divBotonera.classList.add('d-flex', 'justify-content-start', 'flex-row');*/

    
    arasa.boton.ini(obj);

                       
    var strhtml =  arasa.boton.basicform.get_botton_reg_vista();  
    document.getElementById(  obj.tipo + '_acciones' ).innerHTML = strhtml;


    var btn_acciones = document.getElementById('btn_'+ obj.tipo +'_acciones');    
    btn_acciones.onclick =  function() {            
        
        var strhtml =  arasa.boton.basicform.get_botton_reg();  
        document.getElementById(  obj.tipo + '_acciones' ).innerHTML = strhtml;

        


        var btn_nuevo = document.getElementById('btn_'+obj.tipo+'_nuevo');        
        btn_nuevo.onclick =  function() {            
            obj.nuevo( obj );
        };         




        var btn_modificar = document.getElementById('btn_'+ obj.tipo +'_modificar');        
        btn_modificar.onclick =  function() {            
            

            arasa.form.name = "form_" + obj.tipo ;
            arasa.form.campos =  [  obj.tipo + '_'  + obj.campoid ];                 
            
            // solo se desbloquen algunos
            document.getElementById("fecha_pago").disabled =  false;
            
            // Encuentra el elemento de input
            var inputEstadoPago = document.getElementById("estado_pago");
            // Crea un nuevo elemento select
            var selectEstadoPago = document.createElement("select");
            selectEstadoPago.id = inputEstadoPago.id;
            inputEstadoPago.parentNode.replaceChild(selectEstadoPago, inputEstadoPago);      
            // Parsea el JSON
            var data = JSON.parse(pjson);
            // Crea una nueva opción para el select
            var option = document.createElement("option");
            option.value = data.estado_pago.estado_pago;
            option.text = data.estado_pago.descripcion;  
            selectEstadoPago.appendChild(option);            
            //var estadopagovalue = 
            var estado_pago = new EstadoPago();
            estado_pago.combobox("estado_pago", 1);            

            
            document.getElementById("numero_cheque").disabled =  false;
            
            
            
            // tipo cheque
            var inputTipoCheque = document.getElementById("facturaproveedor_tipo_cheque");
            // Crea un nuevo elemento select
            var selectEstadoCheque = document.createElement("select");
            selectEstadoCheque.id = inputTipoCheque.id;
            inputTipoCheque.parentNode.replaceChild(selectEstadoCheque, inputTipoCheque);      
            // Parsea el JSON
            var data = JSON.parse(pjson);
            // Crea una nueva opción para el select
            var option = document.createElement("option");
            option.value = data.tipo_cheque.tipo_cheque;
            option.text = data.tipo_cheque.descripcion;  
            selectEstadoCheque.appendChild(option);            
            //var estadopagovalue = 
            var tipo_cheque = new TipoCheque();
            tipo_cheque.combobox("facturaproveedor_tipo_cheque", 1);              
            
                        
            document.getElementById("dias_diferidos").disabled =  false;
            
            
            arasa.boton.ini(obj);
            document.getElementById(  obj.tipo + '_acciones' ).innerHTML 
                    =  arasa.boton.basicform.get_botton_edit();




            var btn_objeto_editar = document.getElementById('btn_' + obj.tipo + '_editar');
            btn_objeto_editar.onclick =  function() {             
               
                //arasa.ajax.api = arasa.html.url.absolute()  +"/api/" + obj.recurso + "/"+obj.id ;   

                
                if ( obj.validar_edit()) {
                                    
                    arasa.loader.inicio();    

                    arasa.form.name = "form_"+obj.tipo;       
                    
                    var data = JSON.parse(pjson);
                    var id =  data.id;
                    
                    
                    //var id = document.getElementById( obj.tipo + '_'  + obj.campoid ).value;


                    arasa.form.name = "form_"+obj.tipo;
                    arasa.ajax.json = obj.getjson_edit() ; 
                                                           

                    arasa.ajax.api = arasa.html.url.absolute()  +"/api/" + obj.recurso + "/"+id;     
                    
                    
                    arasa.ajax.promise.async("put").then(result => {                
                        json = result.responseText;            

                        if (result.status == 200){

                            obj.tabla_registro(obj.id) ;   
                            arasa.msg.ok.mostrar("Registro agregado"); 
                        }
                        else {                    
                            arasa.msg.error.mostrar(result.responseText); 
                        }
                        arasa.loader.fin();
                    }); 
                    
                    
                }
            }                        


            var btn_objeto_cancelar = document.getElementById('btn_' + obj.tipo + '_cancelar');
            btn_objeto_cancelar.className = 'botonB';
            btn_objeto_cancelar.onclick =  function() {    
                obj.tabla_registro(obj.id) ;               
            }



        };         



        var btn_eliminar = document.getElementById('btn_'+obj.tipo+'_eliminar');        
        btn_eliminar.onclick =  function() {            
            //reflex.acciones.button_del_promise(obj);

            reflex.ini(obj);
            arasa.boton.ini(obj);
            document.getElementById(  obj.tipo + '_acciones' ).innerHTML 
                    =   arasa.boton.basicform.get_botton_del();
                    

            var btn_objeto_eliminar = document.getElementById('btn_' + obj.tipo + '_eliminar');
            btn_objeto_eliminar.onclick =  function() {             
                

                arasa.ajax.api = arasa.html.url.absolute()  +"/api/" + obj.recurso + "/"+obj.id ;   

                                    
                    arasa.loader.inicio();    

                    arasa.ajax.json = null;                     

                    arasa.ajax.api = arasa.html.url.absolute()  +"/api/" + obj.recurso + "/"+id;     
                    arasa.ajax.promise.async("delete").then(result => {                
                        json = result.responseText;            

                        if (result.status == 200){

                            obj.lista(1);
                            arasa.msg.ok.mostrar("Registro eliminado"); 
                        }
                        else {                    
                            arasa.msg.error.mostrar(result.responseText); 
                        }
                        arasa.loader.fin();
                    }); 
                
            }                        






            var btn_objeto_cancelar = document.getElementById('btn_' + obj.tipo + '_cancelar');
            btn_objeto_cancelar.className = 'botonB';
            btn_objeto_cancelar.onclick =  function() {    
                obj.tabla_registro(obj.id) ;               
            }                    


        };         



        
        var btn_lista = document.getElementById('btn_'+obj.tipo+'_lista');
        btn_lista.className = 'botonB';
        btn_lista.onclick =  function() {            
            obj.lista(1);
        }; 
    


    };     



    var btn_atras = document.getElementById('btn_'+obj.tipo+'_atras');
    btn_atras.className = 'botonB';
    btn_atras.onclick =  function() {            
        obj.lista(1);
    }; 

}





FacturaProveedor.prototype.cargar_formulario = function( json ) {

    // Parsear el JSON
    var data = JSON.parse(json);


    // Convierte el string de fecha a objeto Date
    var fecha = fechaYYYYMMDD(data.fecha);
    // Formatea la fecha en el formato requerido por el input de tipo date (YYYY-MM-DD)
    var formattedDate = fecha.getFullYear() + "-" + ("0" + (fecha.getMonth() + 1)).slice(-2) + "-" + ("0" + fecha.getDate()).slice(-2);
    document.getElementById('facturaproveedor_fecha').value = formattedDate;



    var selectOriginal = document.getElementById('facturaproveedor_obra_realizada');
    // Crea un nuevo elemento input
    var inputNuevo = document.createElement('input');
    // Copia los atributos importantes del select al nuevo input
    inputNuevo.type = 'text'; // Cambia el tipo a texto o date según prefieras
    inputNuevo.value = data.obra.descripcion;
    inputNuevo.name = selectOriginal.name;
    inputNuevo.id = selectOriginal.id;
    
    // Reemplaza el select con el nuevo input
    selectOriginal.parentNode.replaceChild(inputNuevo, selectOriginal);

    document.getElementById('anio').value = data.obra.anio;



    var selectOriginal = document.getElementById('facturaproveedor_tipo_factura');
    // Crea un nuevo elemento input
    var inputNuevo = document.createElement('input');
    inputNuevo.type = 'text'; // Cambia el tipo a texto o date según prefieras
    inputNuevo.value = "Proveedor";
    inputNuevo.name = selectOriginal.name;
    inputNuevo.id = selectOriginal.id;
    selectOriginal.parentNode.replaceChild(inputNuevo, selectOriginal);



    document.getElementById('facturaproveedor_timbrado').value = data.timbrado;



    var selectOriginal = document.getElementById('facturaproveedor_tipo_compra');
    // Crea un nuevo elemento input
    var inputNuevo = document.createElement('input');
    inputNuevo.type = 'text'; // Cambia el tipo a texto o date según prefieras
    inputNuevo.value = data.tipo_compra.descripcion;;
    inputNuevo.name = selectOriginal.name;
    inputNuevo.id = selectOriginal.id;
    selectOriginal.parentNode.replaceChild(inputNuevo, selectOriginal);





    var selectOriginal = document.getElementById('facturaproveedor_proveedor');
    // Crea un nuevo elemento input
    var inputNuevo = document.createElement('input');
    inputNuevo.type = 'text'; // Cambia el tipo a texto o date según prefieras
    inputNuevo.value = data.proveedor.descripcion;
    inputNuevo.name = selectOriginal.name;
    inputNuevo.id = selectOriginal.id;
    selectOriginal.parentNode.replaceChild(inputNuevo, selectOriginal);


    document.getElementById('numero_cheque').value = data.numero_cheque;




    var selectOriginal = document.getElementById('estado_pago');
    // Crea un nuevo elemento input
    var inputNuevo = document.createElement('input');
    inputNuevo.type = 'text'; // Cambia el tipo a texto o date según prefieras
    inputNuevo.value = data.estado_pago.descripcion;
    inputNuevo.name = selectOriginal.name;
    inputNuevo.id = selectOriginal.id;
    selectOriginal.parentNode.replaceChild(inputNuevo, selectOriginal);



    // Verificar si fecha_pago es null
    if (data.fecha_pago !== undefined) {
        var fecha = fechaYYYYMMDD(data.fecha_pago);        
        var formattedDate = fecha.getFullYear() + "-" + ("0" + (fecha.getMonth() + 1)).slice(-2) + "-" + ("0" + fecha.getDate()).slice(-2);
        document.getElementById('fecha_pago').value = formattedDate;
    }    
    
    
    
    var selectOriginal = document.getElementById('facturaproveedor_tipo_cheque');
    // Crea un nuevo elemento input
    var inputNuevo = document.createElement('input');
    inputNuevo.type = 'text'; // Cambia el tipo a texto o date según prefieras
    inputNuevo.value = data.tipo_cheque.descripcion;;
    inputNuevo.name = selectOriginal.name;
    inputNuevo.id = selectOriginal.id;
    selectOriginal.parentNode.replaceChild(inputNuevo, selectOriginal);    
    
    
    document.getElementById('dias_diferidos').value = data.dias_diferidos;
    
    

    var indice = 0 ;
    var obj = this;

        // Recorremos los detalles
        data.detalle.forEach(function(detalle) {

            indice++;
            obj.generar_linea_detalle(indice);
            
            
            document.getElementById("borrar_linea_"+indice).remove();    


            var selectMaterial = document.getElementById('material_'+indice);
            var contenedor = selectMaterial.parentNode;
            selectMaterial.remove();
            contenedor.innerHTML = detalle.material.descripcion;  
            

            document.getElementById('unidadmedida_'+indice).innerHTML = detalle.material.unidad_medida.descripcion;  


            var inputcantidad = document.getElementById('cantidad_'+indice);
            var contenedor = inputcantidad.parentNode;
            inputcantidad.remove();
            contenedor.innerHTML = fmtNum(detalle.cantidad);  
            contenedor.style.textAlign = "right";
            
            
            var inputprecio = document.getElementById('precio_unitario_'+indice);
            var contenedor = inputprecio.parentNode;
            inputprecio.remove();
            contenedor.innerHTML = fmtNum(detalle.precio_unitario);  
            contenedor.style.textAlign = "right";            


            var inputsubtotal = document.getElementById('subtotal_'+indice);
            var contenedor = inputsubtotal.parentNode;
            inputsubtotal.remove();
            contenedor.innerHTML = fmtNum(detalle.subtotal);  
            contenedor.style.textAlign = "right";      


        });


        document.getElementById('total_general').innerHTML =  fmtNum(data.total_factura);


}