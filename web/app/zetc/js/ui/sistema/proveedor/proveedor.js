

function Proveedor(){
        
    this.tipo = this.constructor.name.toLowerCase();    
    this.campoid =  this.constructor.name.toLowerCase();    
    this.json = ""; 
    this.tablacampos = ['proveedor', 'descripcion' ];
    this.recurso = "proveedores";
        
    this.page = 1;    
    
}




Proveedor.prototype.lista = function( page ) {        
    
    var obj = this;           
    

    arasa.loader.inicio();    
        
    // tal vez aca llame al la clase            
    fetch('htmf/lista.html')
        .then(response => response.text())
        .then(html => {
            

            document.getElementById( arasa.dom.main ).innerHTML = html;

            arasa.boton.objeto = obj.tipo;


            document.getElementById( 'proveedor_acciones_lista' ).innerHTML 
            =  arasa.boton.basicform.get_botton_att();       



            var btn_agregar = document.getElementById('btn_'+ obj.tipo +'_agregar');              
            btn_agregar.onclick = function(  ) {  
                obj.nuevo();    
            }  

            


            arasa.ajax.api = arasa.html.url.absolute() +  "/api/"+obj.recurso+"?page="+page;
            arasa.ajax.json = null;
            



            arasa.ajax.promise.async("get").then(result => {
                obj.json = result.responseText;
                            


                var ojson = JSON.parse( obj.json ) ; 
                arasa.tabla.json = JSON.stringify(ojson['datos']);     

                arasa.tabla.ini(obj);                
                arasa.tabla.gene();  
                
                //boo.tabla.lista_registro(obj, reflex.form_id_promise ); 
                arasa.tabla.id =  obj.tipo+"_tabla";
                arasa.tabla.lista_registro(obj); 
                
                arasa.vista.paginacion_html(obj,  JSON.stringify(ojson['paginacion']), page  );  
                
                

            });
            
        })
        /*
        .catch(error => {
            console.error('Error al cargar la página:', error);
        })
        */
        .finally(() => {               
            arasa.loader.fin();
        });    

};






Proveedor.prototype.nuevo = function( ) {                
    
    var obj = this;

        
    arasa.loader.inicio();
        
    fetch('htmf/form.html')        
        .then(response => response.text())
        .then(html => {            
            document.getElementById( arasa.dom.main ).innerHTML = html;      
            obj.form_ini();

            return html;
        })
        .then(html => {                
            
            obj.botones_accion_add();            
            
            return html;
        })
        .then(html => {
            //obj.init();
            return html;
        })
        /*
        .catch(error => {
            console.error(error);
        })
        */
        .finally(() => {               
            arasa.loader.fin();
        }); 
     
  

};







Proveedor.prototype.form_ini = function() {    
  
    var codigo = document.getElementById('proveedor_proveedor');            
     codigo.onblur  = function() {                  
         codigo.value = fmtNum(codigo.value);      
    };      
    codigo.disabled = true;
    codigo.onblur();    
    


    var proveedor_cod_factura = document.getElementById('proveedor_cod_factura');            
    proveedor_cod_factura.onblur  = function() {                  
        proveedor_cod_factura.value = fmtNum(proveedor_cod_factura.value);      
    };      
    proveedor_cod_factura.onblur();    

    
    
};











Proveedor.prototype.botones_accion_add = function( ) {    
    var obj = this;
    

    arasa.boton.objeto = obj.tipo;
    document.getElementById( obj.tipo + '_acciones' ).innerHTML 
    =  arasa.boton.basicform.get_botton_add();       
    



    

    var btn_cancelar = document.getElementById('btn_'+obj.tipo+'_cancelar');
    btn_cancelar.className = 'botonB';
    btn_cancelar.onclick =  function() {    
        
        obj.lista(1);
    }; 




    var btn_guardar = document.getElementById('btn_'+obj.tipo+'_guardar');
    btn_guardar.addEventListener('click', function() {
        
        //let obj = new Cliente();                
        if ( obj.validar()){      
                        
            
            document.getElementById( 'proveedor_cod_factura' ).value = 1;
            
            
            arasa.form.name = "form_"+obj.tipo;
                        
            arasa.ajax.api = arasa.html.url.absolute() +  "/api/" +obj.recurso
            arasa.ajax.json = arasa.form.datos.getjson() ; 

         
            
            arasa.ajax.promise.async("post").then(result => {                
                json = result.responseText;            

                if (result.status == 200){
                    
                    obj.lista(1);
                    
                    arasa.msg.ok.mostrar("Registro agregado"); 
                }
                else {                    
                    arasa.msg.error.mostrar(result.responseText); 
                }

            });  

        }      
    });    
    
    

}







Proveedor.prototype.validar = function() {   
    
    
    var descripcion = document.getElementById('proveedor_descripcion');    
    if (descripcion.value.trim() === "")         
    {        
        arasa.msg.error.mostrar("La descripcion no puede estar vacia"); 
        descripcion.focus();
        descripcion.select();        
        return false;
    }       

    return true;
    
};





Proveedor.prototype.tabla_registro = function( id ) {    

    var obj = this;
   
    arasa.loader.inicio();

    
    fetch('htmf/form.html')   
        .then(response => response.text())
        .then(html => {
            
            document.getElementById( arasa.dom.main ).innerHTML = html;
            
            arasa.ajax.api = arasa.html.url.absolute() +  "/api/"+obj.recurso+"/"+id;            
            arasa.ajax.promise.async("get").then(result => {
                obj.json = result.responseText;                                


                // cargar form
                arasa.form.name = "form_"+ obj.tipo;
                arasa.form.json = obj.json;   
                arasa.form.disabled(false);   
                arasa.form.llenar();                


                obj.botones_accion_registro(id); 
            });
            

        })
        /*
        .catch(error => {
            console.error('Error al cargar la página:', error);
        })
        */
        .finally(() => {               
            arasa.loader.fin();
        });  

    
};










Proveedor.prototype.botones_accion_registro = function( id) {  

    var obj = this;
    obj.id = id;
        
    var divBotonera = document.getElementById('botonera');
    /*divBotonera.classList.add('d-flex', 'justify-content-start', 'flex-row');*/

    
    arasa.boton.ini(obj);

                       
    var strhtml =  arasa.boton.basicform.get_botton_reg_vista();  
    document.getElementById(  obj.tipo + '_acciones' ).innerHTML = strhtml;


    var btn_acciones = document.getElementById('btn_'+obj.tipo+'_acciones');    
    btn_acciones.onclick =  function() {            
        
        var strhtml =  arasa.boton.basicform.get_botton_reg();  
        document.getElementById(  obj.tipo + '_acciones' ).innerHTML = strhtml;

        


        var btn_nuevo = document.getElementById('btn_'+obj.tipo+'_nuevo');        
        btn_nuevo.onclick =  function() {            
            obj.nuevo( obj );
        };         




        var btn_modificar = document.getElementById('btn_'+obj.tipo+'_modificar');        
        btn_modificar.onclick =  function() {            
            

            arasa.form.name = "form_" + obj.tipo ;
            arasa.form.campos =  [  obj.tipo + '_'  + obj.campoid ];                 
            arasa.form.disabled(true);




            arasa.boton.ini(obj);
            document.getElementById(  obj.tipo + '_acciones' ).innerHTML 
                    =  arasa.boton.basicform.get_botton_edit();




            var btn_objeto_editar = document.getElementById('btn_' + obj.tipo + '_editar');
            btn_objeto_editar.onclick =  function() {             

               

                arasa.ajax.api = arasa.html.url.absolute()  +"/api/" + obj.recurso + "/"+obj.id ;   


                if ( obj.validar()) {
                                    
                    arasa.loader.inicio();    

                    arasa.form.name = "form_"+obj.tipo;                        
                    var id = document.getElementById( obj.tipo + '_'  + obj.campoid ).value;


                    arasa.form.name = "form_"+obj.tipo;
                    arasa.ajax.json = arasa.form.datos.getjson() ; 
                    

                    arasa.ajax.api = arasa.html.url.absolute()  +"/api/" + obj.recurso + "/"+id;     
                    arasa.ajax.promise.async("put").then(result => {                
                        json = result.responseText;            

                        if (result.status == 200){

                            obj.tabla_registro(obj.id) ;   
                            arasa.msg.ok.mostrar("Registro agregado"); 
                        }
                        else {                    
                            arasa.msg.error.mostrar(result.responseText); 
                        }
                        arasa.loader.fin();
                    }); 
                }
            }                        


            var btn_objeto_cancelar = document.getElementById('btn_' + obj.tipo + '_cancelar');
            btn_objeto_cancelar.className = 'botonB';
            btn_objeto_cancelar.onclick =  function() {    
                obj.tabla_registro(obj.id) ;               
            }



        };         



        var btn_eliminar = document.getElementById('btn_'+obj.tipo+'_eliminar');        
        btn_eliminar.onclick =  function() {            
            //reflex.acciones.button_del_promise(obj);

            reflex.ini(obj);
            arasa.boton.ini(obj);
            document.getElementById(  obj.tipo + '_acciones' ).innerHTML 
                    =   arasa.boton.basicform.get_botton_del();
                    

            var btn_objeto_eliminar = document.getElementById('btn_' + obj.tipo + '_eliminar');
            btn_objeto_eliminar.onclick =  function() {             
                

                arasa.ajax.api = arasa.html.url.absolute()  +"/api/" + obj.recurso + "/"+obj.id ;   

                                    
                    arasa.loader.inicio();    

                    arasa.ajax.json = null;                     

                    arasa.ajax.api = arasa.html.url.absolute()  +"/api/" + obj.recurso + "/"+id;     
                    arasa.ajax.promise.async("delete").then(result => {                
                        json = result.responseText;            

                        if (result.status == 200){

                            obj.lista(1);
                            arasa.msg.ok.mostrar("Registro eliminado"); 
                        }
                        else {                    
                            arasa.msg.error.mostrar(result.responseText); 
                        }
                        arasa.loader.fin();
                    }); 
                
            }                        






            var btn_objeto_cancelar = document.getElementById('btn_' + obj.tipo + '_cancelar');
            btn_objeto_cancelar.className = 'botonB';
            btn_objeto_cancelar.onclick =  function() {    
                obj.tabla_registro(obj.id) ;               
            }                    


        };         



        
        var btn_lista = document.getElementById('btn_'+obj.tipo+'_lista');
        btn_lista.className = 'botonB';
        btn_lista.onclick =  function() {            
            obj.lista(1);
        }; 
    


    };     



    var btn_atras = document.getElementById('btn_'+obj.tipo+'_atras');
    btn_atras.className = 'botonB';
    btn_atras.onclick =  function() {            
        obj.lista(1);
    }; 

}









Proveedor.prototype.combobox = function( domid, num ) {                   

    var dom = document.getElementById( domid );
    var idedovalue = dom.value;


    arasa.ajax.api = arasa.html.url.absolute()  +  "/api/proveedores/all"        
    arasa.ajax.promise.async("get").then(result => {


        var ojson = JSON.parse( result.responseText ) ;    
        for( x=0; x < ojson.length; x++ ) {

            var jsonvalue = (ojson[x]['proveedor'] );            

            if (idedovalue != jsonvalue )
            {  
                var opt = document.createElement('option');            
                opt.value = jsonvalue;
                opt.innerHTML = ojson[x]['descripcion'];                        
                dom.appendChild(opt);                     
            }
        }    
        
        
        // Inicializa Choices.js en el elemento <select>
        var choices = new Choices(dom, {
            position: 'bottom',            
            /* Opciones adicionales según tus necesidades */
        });      
        
        
        
        /*
        if ( num != 0 ) {            
            // Obtener el elemento padre del elemento hijo
            var comboinput = dom.parentNode.children[1];
            comboinput.textContent = '';
            comboinput.setAttribute('data-id', '0');
            comboinput.setAttribute('data-value', '0');
            comboinput.id = 'choices_'+dom.id;          
        }             
        */
        
        
    });


};


