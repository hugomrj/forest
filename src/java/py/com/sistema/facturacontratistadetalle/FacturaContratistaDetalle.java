/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.sistema.facturacontratistadetalle;

import py.com.sistema.materiales_manoobra.MaterialManoObra;

/**
 *
 * @author hugo
 */
public class FacturaContratistaDetalle {
    
    private Integer id;
    private Integer factura_contratista;
    private MaterialManoObra material;
    private Long cantidad;
    private Long precio_unitario;
    private Long subtotal;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getFactura_contratista() {
        return factura_contratista;
    }

    public void setFactura_contratista(Integer factura_contratista) {
        this.factura_contratista = factura_contratista;
    }

    public MaterialManoObra getMaterial() {
        return material;
    }

    public void setMaterial(MaterialManoObra material) {
        this.material = material;
    }

    public Long getCantidad() {
        return cantidad;
    }

    public void setCantidad(Long cantidad) {
        this.cantidad = cantidad;
    }

    public Long getPrecio_unitario() {
        return precio_unitario;
    }

    public void setPrecio_unitario(Long precio_unitario) {
        this.precio_unitario = precio_unitario;
    }

    public Long getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(Long subtotal) {
        this.subtotal = subtotal;
    }
    
    
}





