/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sistema.consulta;

import py.com.sistema.contratista.*;
import nebuleuse.ORM.sql.ReaderT;


/**
 *
 * @author hugo
 */
public class ConsultaSQL {
    
    
        
     
    public String consulta1 (Integer obra )
            throws Exception {
    
        String sql = "";                                 
        
        
        
        
        sql = """

        SELECT 1 as orden, material as codigo, mm.descripcion materialmano, 
          um.descripcion uni_medida, sum(fcd.cantidad) cantidad , sum(fcd.subtotal) total 
          FROM facturas_contratistas fc inner join facturas_contratistas_detalles fcd
          on (fc.id = fcd.factura_contratista) 
          inner join materiales_manoobra mm on (fcd.material = mm.id)
          inner join unidades_medidas um on (mm.unidad_medida = um.unidad_medida)
          where obra = numero_codigo_obra
          group by orden, codigo, materialmano, uni_medida

          union 

          SELECT 2 as orden, material as codigo, mm.descripcion materialmano, 
          um.descripcion uni_medida, sum(fpd.cantidad) cantidad , sum(fpd.subtotal) total 
          FROM facturas_proveedores fp inner join facturas_proveedores_detalles fpd 
          on (fp.id = fpd.factura_proveedor) 
          inner join materiales_manoobra mm on (fpd.material = mm.id)
          inner join unidades_medidas um on (mm.unidad_medida = um.unidad_medida)
          where obra = numero_codigo_obra
          group by orden, codigo, materialmano, uni_medida

          
        """;        
        
        
        sql = sql.replace("numero_codigo_obra", obra.toString()) ; 
        
        return sql ;             
    
    }   
       
    
    
}
