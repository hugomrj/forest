/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sistema.consulta;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import nebuleuse.ORM.db.Coleccion;
import nebuleuse.ORM.db.JsonObjeto;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.ORM.db.RegistroMap;
import nebuleuse.ORM.db.ResultadoSet;
import nebuleuse.ORM.sql.SentenciaSQL;



public class ConsultaJSON  {

    Persistencia persistencia = new Persistencia();  
    
    
    public ConsultaJSON ( ) throws IOException  {
        
        
    }
      
    
    

    public String  consulta1 (Integer obra ) {
        
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();                
        JsonObject jsonObject = new JsonObject();
        String jsonlista = ""; 
        
        try 
        {               
            
            ResultadoSet resSet = new ResultadoSet();                      
            
            String sql = "";
            String sqlFiltro = "";            
            String sqlOrder = "";                 
            
            sql = new ConsultaSQL().consulta1(obra);     
            
            String sql_suma = " select sum(total) as total_general from ( "
                    + sql + " ) as t";
            
            
            //sql = sql + sqlFiltro + sqlOrder ;           
            
            ResultSet rsData = resSet.resultset(sql);                            
            
            
            

            JsonArray jsonArrayDatos = new JsonArray();

            while (rsData.next()) {
                // Crear un HashMap para almacenar los datos del registro
                HashMap<String, Object> registro = new HashMap<>();
                
                // Suponiendo que RegistroMap es una clase que convierte el ResultSet a HashMap
                RegistroMap registroMap = new RegistroMap();      
                registro = registroMap.convertirHashMap(rsData);           


                HashMap<String, String> registroString = new HashMap<>();
                // Iterar sobre las entradas del HashMap original
                for (Map.Entry<String, Object> entry : registro.entrySet()) {
                    String key = entry.getKey();
                    Object value = entry.getValue();

                    // Comprobar si el valor es numérico
                    try {
                        Double.parseDouble(value.toString());
                        registroString.put(key, value.toString());
                    } catch (NumberFormatException e) {
                        registroString.put(key, value.toString());
                    }
                }
                
                
                // Convertir el HashMap de cadenas a un JsonObject
                JsonObject jsonRegistro = gson.toJsonTree(registroString).getAsJsonObject();

                // Agregar el JsonObject al JsonArray
                jsonArrayDatos.add(jsonRegistro);
            }            
            
            
            ResultSet rsDataSuma = resSet.resultset(sql_suma);
            JsonObject jsonSuma = new JsonObject();
            if (rsDataSuma.next()) {
                // Obtener el valor del campo "total_general" del ResultSet
                int totalGeneral = rsDataSuma.getInt("total_general");

                // Crear un objeto JsonObject
                

                // Agregar el campo "total_general" al objeto JsonObject
                jsonSuma.addProperty("total_general", totalGeneral);

                // Convertir el JsonObject a una cadena JSON
                String json = new Gson().toJson(jsonSuma);

                // Imprimir la cadena JSON
                System.out.println(json);
            }
            
            
            
            
            
            jsonObject.add("datos", jsonArrayDatos);   
            jsonObject.add("suma", jsonSuma);   
            jsonlista = jsonObject.toString();
            
        }         
        catch (Exception ex) {                   
            
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {            
            return jsonlista ;         
        }
    }      
    
    
    


    
        
}
