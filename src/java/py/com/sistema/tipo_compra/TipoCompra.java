package py.com.sistema.tipo_compra;



public class TipoCompra {

    private Integer tipo_compra;
    private String descripcion;    

    public Integer getTipo_compra() {
        return tipo_compra;
    }

    public void setTipo_compra(Integer tipo_compra) {
        this.tipo_compra = tipo_compra;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    
    
}
