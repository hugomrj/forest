/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
 
package py.com.sistema.oficio;

public class Oficio {
    
    private Integer oficio;
    private String descripcion;

    public Integer getOficio() {
        return oficio;
    }

    public void setOficio(Integer oficio) {
        this.oficio = oficio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

}

