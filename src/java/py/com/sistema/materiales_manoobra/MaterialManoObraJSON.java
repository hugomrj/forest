/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sistema.materiales_manoobra;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.List;
import nebuleuse.ORM.db.Coleccion;
import nebuleuse.ORM.db.JsonObjeto;
import nebuleuse.ORM.db.ResultadoSet;
import nebuleuse.ORM.sql.SentenciaSQL;



public class MaterialManoObraJSON  {


    
    
    public MaterialManoObraJSON ( ) throws IOException  {
        
        
        
    }
      
            

    public JsonObject  lista ( Integer page, String buscar ) {
        
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();                
        JsonObject jsonObject = new JsonObject();
        
        try 
        {               
            
            ResultadoSet resSet = new ResultadoSet();                      
            
            String sql = "";
            String sqlFiltro = "";            
            String sqlOrder = "";
            
            if (buscar == null) {                
                sql = SentenciaSQL.select(new MaterialManoObra());    
            }
            else {                
                sql =  SentenciaSQL.select(new MaterialManoObra());   
                sqlFiltro =  new MaterialManoObraSQL().filtro(buscar);   ;       
            }    
            // sqlOrder =  " order by id";            
            
            sql = sql + sqlFiltro + sqlOrder ;          
            
            
       
            

            ResultSet rsData = resSet.resultset(sql, page);                            
            
            List<MaterialManoObra>  lista = new Coleccion<MaterialManoObra>().resultsetToList(
                    new MaterialManoObra(),
                    rsData );     
            String jsonlista = gson.toJson( lista ); 
            
            //JsonArray jsonarrayDatos = new JsonArray();
            JsonArray jsonarrayDatos = JsonParser.parseString(jsonlista).getAsJsonArray();
            
            // paginacipon
            JsonObject jsonPaginacion = new JsonObject();            
            jsonPaginacion = new JsonObjeto().json_paginacion(sql, page);
                        
            // union de partes
            jsonObject.add("paginacion", jsonPaginacion);            
            jsonObject.add("datos", jsonarrayDatos);    
            //jsonObject.add("summary", jsonarraySuma);        
            
            
            resSet.close();
        }         
        catch (Exception ex) {                   
            
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return jsonObject ;         
        }
    }      
    
    
    
    


    public String  all ( ) {
        
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();                
        JsonObject jsonObject = new JsonObject();
        String jsonlista = ""; 
        
        try 
        {                           
            ResultadoSet resSet = new ResultadoSet();                      
            
            String sql = "";
            String sqlFiltro = "";            
            String sqlOrder = "";                                
            sql = SentenciaSQL.select(new MaterialManoObra());    
            
            sqlOrder =  " order by id";            
            sql = sql + sqlFiltro + sqlOrder ;           
            
            ResultSet rsData = resSet.resultset(sql);                            
            
            List<MaterialManoObra>  lista = new Coleccion<MaterialManoObra>().resultsetToList(
                    new MaterialManoObra(),
                    rsData );    
                        
            jsonlista = gson.toJson( lista ); 
            
            resSet.close();
        }         
        catch (Exception ex) {                   
            
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {      
            return jsonlista ;         
        }
    }      
    




    public String  all ( Integer tipo ) {
        
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();                
        JsonObject jsonObject = new JsonObject();
        String jsonlista = ""; 
        
        try 
        {                           
            ResultadoSet resSet = new ResultadoSet();                      
            
            String sql = "";
            String sqlFiltro = "";            
            String sqlOrder = "";                                
            sql = SentenciaSQL.select(new MaterialManoObra());    

            sqlFiltro = " WHERE tipo_usuario =  " + tipo ;
            
            sqlOrder =  " order by id";            
            sql = sql + sqlFiltro + sqlOrder ;           
            

            ResultSet rsData = resSet.resultset(sql);                            
            
            List<MaterialManoObra>  lista = new Coleccion<MaterialManoObra>().resultsetToList(
                    new MaterialManoObra(),
                    rsData );    
                        
            jsonlista = gson.toJson( lista ); 
            
            resSet.close();
        }         
        catch (Exception ex) {                   
            
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {      
            return jsonlista ;         
        }
    }      
    
        
    
    
    
    
    
    
    
        
}
