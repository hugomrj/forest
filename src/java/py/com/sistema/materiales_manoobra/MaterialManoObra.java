/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
 
package py.com.sistema.materiales_manoobra;

import py.com.sistema.tipo_usuario.TipoUsuario;
import py.com.sistema.unidad_medida.UnidadMedida;




public class MaterialManoObra {
    
    private Integer id;
    private UnidadMedida unidad_medida;
    private TipoUsuario tipo_usuario;
    private String descripcion;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public UnidadMedida getUnidad_medida() {
        return unidad_medida;
    }

    public void setUnidad_medida(UnidadMedida unidad_medida) {
        this.unidad_medida = unidad_medida;
    }

    public TipoUsuario getTipo_usuario() {
        return tipo_usuario;
    }

    public void setTipo_usuario(TipoUsuario tipo_usuario) {
        this.tipo_usuario = tipo_usuario;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
}

