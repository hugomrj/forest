package py.com.sistema.tipo_usuario;


/**
 *
 * @author hugo
 */


public class TipoUsuario {

    public Integer getTipo_usuario() {
        return tipo_usuario;
    }

    public void setTipo_usuario(Integer tipo_usuario) {
        this.tipo_usuario = tipo_usuario;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    private Integer tipo_usuario;
    private String descripcion;    

    
    
}
