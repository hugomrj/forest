/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package py.com.sistema.facturacontratista;




import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.sql.SQLException;

import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.UriInfo;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.MatrixParam;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.seguridad.Autentificacion;
import org.json.JSONObject;


/**
 * REST Web Service
 * @author hugo
 */


@Path("facturacontratista")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)



public class FacturaContratistaWS {

    @Context
    private UriInfo context;    
    private Persistencia persistencia = new Persistencia();   
    private Autentificacion autorizacion = new Autentificacion();
    private Gson gson = new Gson();          
    private Response.Status status  = Response.Status.OK;
    
    FacturaContratista com = new FacturaContratista();       
                         
    
    
    public FacturaContratistaWS() {
    }

    
    
    
    @GET    
    public Response lista ( 
            @HeaderParam("token") String strToken,
            @MatrixParam("q") String q,            
            @QueryParam("page") Integer page) {
        
        
            if (page == null) {                
                page = 1;
            }

        try {                    
           
            //if (autorizacion.verificar(strToken))
            if (true)
            {                
                // autorizacion.actualizar();                                
                
                JsonObject jsonObject ;                                                
                if (q == null) {                                
                    jsonObject = new FacturaContratistaJSON().lista(page, null);
                } 
                else{
                    jsonObject = new FacturaContratistaJSON().lista(page, q);
                }                
                
                
                return Response
                        .status(Response.Status.OK)
                        .entity(jsonObject.toString())
                        //.header("token", autorizacion.encriptar())
                        .build();                       
            }
            else{
                
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();   
            }        
        }     
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", null)
                    .build();                                        
        }      
    }    
    
      
    
    

    
    
    
    
    
 
    @POST
    @Path("/facturar")
    public Response add( 
            @HeaderParam("token") String strToken,
            String json ) {
                     
        
        try {                    
                       
            if (true)
            //if (autorizacion.verificar(strToken))
            {                
                //autorizacion.actualizar();  
             
        
                FacturaContratistaDAO dao = new FacturaContratistaDAO();

                Integer codigo_venta = dao.insertar(json);

                
                /*
                // envio a la sifen
                ConectorSOAP soap = new ConectorSOAP();        
                soap.insertar(codigo_venta);
                */
                
                
                /*
                if (this.venta == null){
                    this.status = Response.Status.NO_CONTENT;
                }                                
                */
                
                
                return Response
                        .status(this.status)
                        .entity(codigo_venta)
                        .header("token", autorizacion.encriptar())
                        .build();                       
            }
            else
            {                                
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();                   
                
            }
        
        }     
        catch (Exception ex) {
            
            System.err.println(ex.getMessage());
            
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(ex.getMessage())
                    .header("token", autorizacion.encriptar())
                    .build();                                        
        }        

    }    
 
    
            
    
    
    
    
    @GET
    @Path("/{id}")
    public Response get(     
            @HeaderParam("token") String strToken,
            @PathParam ("id") Integer id ) {
                     
        try 
        {                  
            if (true)
            //if (autorizacion.verificar(strToken))
            {
                //autorizacion.actualizar();       
                               
                FacturaContratistaJSON facturaJson = new FacturaContratistaJSON();
                
                JSONObject json  = facturaJson.registro(id);
                                
                if (this.com == null){
                    this.status = Response.Status.NO_CONTENT;                           
                }
                
                return Response
                        .status( this.status )                        
                        .entity(json.toString())
                        .header("token", autorizacion.encriptar())
                        .build();       
            }
            else{
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();   
            }
        
        }     
        catch (Exception ex) {            
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(ex.getMessage())
                    .header("token", autorizacion.encriptar())
                    .build();           
        }          
    }    
      
        
        
    
    
    
    
    
    

    @DELETE  
    @Path("/{id}")    
    public Response delete (            
            @HeaderParam("token") String strToken,
            @PathParam ("id") Integer id) {
            
        try {                    
           
            //if (autorizacion.verificar(strToken))
            if (true)                
            {                
                //autorizacion.actualizar();    
            
                Integer filas = 0;
                //filas = persistencia.delete(this.com, id) ;
                filas = new FacturaContratistaDAO().eliminar(id);
                                
                if (filas != 0){
                    
                    return Response
                            .status(Response.Status.OK)
                            .entity(null)
                            .header("token", autorizacion.encriptar())
                            .build();                       
                }
                else{                    
                    
                    return Response
                            .status(Response.Status.NO_CONTENT)
                            .entity(null)
                            .header("token", autorizacion.encriptar())
                            .build();          
                    
                    
                }
            }
            else
            {  
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();              
            }        
        } 

        catch (Exception ex) {            
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(ex.getMessage())
                    .header("token", autorizacion.encriptar())
                    .build();           
        }  
        
    }    
        
    
    
    
 
         

    @PUT    
    @Path("/{id}")    
    public Response edit (            
            @HeaderParam("token") String strToken,
            @PathParam ("id") Integer id,
            String json 
            ) {

        try {                    
           
            //if (autorizacion.verificar(strToken))
            if (true)
            {                
                //autorizacion.actualizar();    

                FacturaContratistaDAO dao = new FacturaContratistaDAO();

                Integer codigo_venta = dao.editar(json, id);
                
                
                /*
                this.com = (ObraRealizada) persistencia.update(req);
                json = gson.toJson(this.com);    
            */
                
                return Response
                        .status(Response.Status.OK)
                        .entity(json)
                        .header("token", autorizacion.encriptar())
                        .build();       
            }
            else{    
            
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();                   
                
            }
        
        }             
        catch (Exception ex) {

            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", autorizacion.encriptar())
                    .build();         
        }        
        
        
        
    }    
    
    
    

    
    
    
    
    
    
    
    
}