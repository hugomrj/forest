/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.sistema.facturacontratista;


import java.util.Date;
import py.com.sistema.contratista.Contratista;
import py.com.sistema.estado_pago.EstadoPago;
import py.com.sistema.obrarealizada.ObraRealizada;
import py.com.sistema.tipo_cheque.TipoCheque;
import py.com.sistema.tipo_compra.TipoCompra;

/**
 *
 * @author hugo
 */
public class FacturaContratista {
    
    private Integer id;
    private Date fecha;
    private ObraRealizada obra;
    private String numero_factura;
    private String timbrado;
    private TipoCompra tipo_compra;
    private Contratista contratista;
    private String ruc;
    private Date fecha_pago;
    private String numero_cheque;
    private EstadoPago estado_pago;
    private Long total_factura;
    private TipoCheque tipo_cheque;
    private Integer dias_diferidos;
    

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public ObraRealizada getObra() {
        return obra;
    }

    public void setObra(ObraRealizada obra) {
        this.obra = obra;
    }

    public String getNumero_factura() {
        return numero_factura;
    }

    public void setNumero_factura(String numero_factura) {
        this.numero_factura = numero_factura;
    }

    public String getTimbrado() {
        return timbrado;
    }

    public void setTimbrado(String timbrado) {
        this.timbrado = timbrado;
    }

    public TipoCompra getTipo_compra() {
        return tipo_compra;
    }

    public void setTipo_compra(TipoCompra tipo_compra) {
        this.tipo_compra = tipo_compra;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public Date getFecha_pago() {
        return fecha_pago;
    }

    public void setFecha_pago(Date fecha_pago) {
        this.fecha_pago = fecha_pago;
    }

    public String getNumero_cheque() {
        return numero_cheque;
    }

    public void setNumero_cheque(String numero_cheque) {
        this.numero_cheque = numero_cheque;
    }

    public EstadoPago getEstado_pago() {
        return estado_pago;
    }

    public void setEstado_pago(EstadoPago estado_pago) {
        this.estado_pago = estado_pago;
    }

    public Long getTotal_factura() {
        return total_factura;
    }

    public void setTotal_factura(Long total_factura) {
        this.total_factura = total_factura;
    }

    public TipoCheque getTipo_cheque() {
        return tipo_cheque;
    }

    public void setTipo_cheque(TipoCheque tipo_cheque) {
        this.tipo_cheque = tipo_cheque;
    }

    public Integer getDias_diferidos() {
        return dias_diferidos;
    }

    public void setDias_diferidos(Integer dias_diferidos) {
        this.dias_diferidos = dias_diferidos;
    }

    public Contratista getContratista() {
        return contratista;
    }

    public void setContratista(Contratista contratista) {
        this.contratista = contratista;
    }


    
    
    
    
    
}
