/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sistema.facturacontratista;


import nebuleuse.ORM.sql.ReaderT;


/**
 *
 * @author hugo
 */
public class FacturaContratistaSQL {
    
    
        
     
    public String filtro (String buscar )
            throws Exception {
    
        String sql = "";                                 
        
        if (buscar != null) {
            buscar = buscar.replace(" ", "%") ;    
        }        
        
        ReaderT reader = new ReaderT("FacturaContratista");
        reader.fileExt = "filtro.sql";
        
        sql = reader.get( buscar );    
        
        return sql ;             
    
    }   
    
    
    
    
    
    
    
    public String calculo_totales ( Integer codigo ) 
            throws Exception {
    
        String sql = "";                  
                

        sql = """                        
              
            UPDATE public.facturas_contratistas
              	SET total_factura = (
              		SELECT sum(subtotal) total
              		FROM public.facturas_contratistas_detalles
              		where factura_contratista =  %s	)
              	WHERE id = %s;                            
                         
        """;              

            
        sql = String.format(sql, codigo, codigo );
        
        
        return sql ;                 
    }      
            
            
    
        
       
    
    
}
