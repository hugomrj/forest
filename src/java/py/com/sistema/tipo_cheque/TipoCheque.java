package py.com.sistema.tipo_cheque;


public class TipoCheque {

    private Integer tipo_cheque;
    private String descripcion;    

    public Integer getTipo_cheque() {
        return tipo_cheque;
    }

    public void setTipo_cheque(Integer tipo_cheque) {
        this.tipo_cheque = tipo_cheque;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }


    
    
}
