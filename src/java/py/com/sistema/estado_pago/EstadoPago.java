package py.com.sistema.estado_pago;



public class EstadoPago {

    private Integer estado_pago;
    private String descripcion;    

    public Integer getEstado_pago() {
        return estado_pago;
    }

    public void setEstado_pago(Integer estado_pago) {
        this.estado_pago = estado_pago;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    
}
