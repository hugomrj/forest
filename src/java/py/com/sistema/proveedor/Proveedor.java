/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
 
package py.com.sistema.proveedor;



public class Proveedor {
    
    private Integer proveedor;
    private String descripcion;
    private Integer cod_factura;

    public Integer getProveedor() {
        return proveedor;
    }

    public void setProveedor(Integer proveedor) {
        this.proveedor = proveedor;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getCod_factura() {
        return cod_factura;
    }

    public void setCod_factura(Integer cod_factura) {
        this.cod_factura = cod_factura;
    }
    
}

