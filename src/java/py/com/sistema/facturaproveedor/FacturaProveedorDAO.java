
package py.com.sistema.facturaproveedor;


import nebuleuse.ORM.db.Persistencia;
import org.json.JSONObject;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import org.json.JSONArray;
import py.com.sistema.estado_pago.EstadoPago;
import py.com.sistema.facturaproveedordetalle.FacturaProveedorDetalleDAO;
import py.com.sistema.obrarealizada.ObraRealizada;
import py.com.sistema.proveedor.Proveedor;
import py.com.sistema.tipo_cheque.TipoCheque;
import py.com.sistema.tipo_compra.TipoCompra;



public class FacturaProveedorDAO {
        
    Persistencia persistencia = new Persistencia();  
    FacturaProveedor com = new FacturaProveedor();
    
    
    public Integer insertar( String json) throws Exception {
           
        

        FacturaProveedor factura = new FacturaProveedor();                
        factura.setId(0);
                
        JSONObject jsonObject = new JSONObject(json);
        
        // fecha
        String fecha = jsonObject.getString("fecha");
        LocalDate fechaF = LocalDate.parse(fecha, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        Date fechaDate = java.sql.Date.valueOf(fechaF);  
        factura.setFecha(fechaDate);        
        
        
        //ObraRealizada
        ObraRealizada obrarealizada = new ObraRealizada();
        obrarealizada.setCodigo( jsonObject.getInt("obra")); 
        factura.setObra(obrarealizada);
        
        
        factura.setNumero_factura( jsonObject.getString("numero_factura"));
        factura.setTimbrado(jsonObject.getString("timbrado"));
        factura.setRuc(jsonObject.getString("ruc"));
        factura.setNumero_cheque(jsonObject.getString("numero_cheque"));
        
        
        //TipoCompra
        TipoCompra tipo_compra = new TipoCompra();
        tipo_compra.setTipo_compra( jsonObject.getInt("tipo_compra") );
        factura.setTipo_compra(tipo_compra);
        
        
        //Proveedor
        Proveedor proveedor = new Proveedor();
        proveedor.setProveedor( jsonObject.getInt("proveedor") );
        factura.setProveedor(proveedor);
        
        

        // fecha pago
        String fecha_pago = jsonObject.getString("fecha_pago");
        if (!fecha_pago.isEmpty()) {
            LocalDate fecha_pagoF = LocalDate.parse(fecha_pago, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            Date fecha_pagoDate = java.sql.Date.valueOf(fecha_pagoF);  
            factura.setFecha_pago(fecha_pagoDate);        
        }         
        
                
        // estado pago        
        EstadoPago estado_pago = new EstadoPago();
        estado_pago.setEstado_pago( jsonObject.getInt("estado_pago") );
        factura.setEstado_pago(estado_pago);
                      
        
        //TipoCheque
        TipoCheque tipo_cheque = new TipoCheque();
        tipo_cheque.setTipo_cheque(jsonObject.getInt("tipo_cheque") );
        factura.setTipo_cheque(tipo_cheque);
        
        factura.setDias_diferidos(jsonObject.getInt("dias_diferidos"));
        
        
        
        
        // insertar cabecera
        factura = (FacturaProveedor) this.persistencia.insert(factura) ;              
        
        
        
        // detalles         
        JSONArray detalleArray = jsonObject.getJSONArray("detalle");
        
        
        FacturaProveedorDetalleDAO detalleDAO = new FacturaProveedorDetalleDAO();                  
        detalleDAO.insertardet( factura.getId(),  detalleArray);        
        
        
        // calculos totales
        this.calculo_totales(factura);        
        

        return factura.getId();
    }
    
    
    
    
    
    
    public boolean calculo_totales ( FacturaProveedor factura ) 
            throws Exception {
    
        String sql = new FacturaProveedorSQL().calculo_totales( factura.getId() );        
        
        boolean bool = persistencia.ejecutarSQL(sql);
        
        return bool;
        
    }      
    
    
    
    
    
    public Integer eliminar( Integer id) throws Exception {
                    
        FacturaProveedorDetalleDAO daoD = new  FacturaProveedorDetalleDAO();       
        Integer borrados = daoD.eliminar_detalle(id);
                
        Integer borrado = persistencia.delete(this.com, id);
    
        return borrado;
    }
    
    
    
    public Integer editar( String json, Integer id) throws Exception {    
        
        
        FacturaProveedor factura = new FacturaProveedor();                
        factura.setId(id);
        
        JSONObject jsonObject = new JSONObject(json);
        
        factura  = (FacturaProveedor) persistencia.filtrarId(factura, id);  
    
        // fecha pago
        String fecha_pago = jsonObject.getString("fecha_pago");
        if (!fecha_pago.isEmpty()) {
            LocalDate fecha_pagoF = LocalDate.parse(fecha_pago, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            Date fecha_pagoDate = java.sql.Date.valueOf(fecha_pagoF);  
            factura.setFecha_pago(fecha_pagoDate);        
        }    
        
        
        factura.setNumero_cheque(jsonObject.getString("numero_cheque"));
        
        
        // estado pago        
        EstadoPago estado_pago = new EstadoPago();
        estado_pago.setEstado_pago( jsonObject.getInt("estado_pago") );
        factura.setEstado_pago(estado_pago);
        

        //TipoCheque
        TipoCheque tipo_cheque = new TipoCheque();
        tipo_cheque.setTipo_cheque(jsonObject.getInt("tipo_cheque") );
        factura.setTipo_cheque(tipo_cheque);
        
        factura.setDias_diferidos(jsonObject.getInt("dias_diferidos"));

        
        factura = (FacturaProveedor) persistencia.update(factura);
        
        
        return id;
    }        
    
}















