/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sistema.facturaproveedor;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.List;
import nebuleuse.ORM.db.Coleccion;
import nebuleuse.ORM.db.JsonObjeto;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.ORM.db.ResultadoSet;
import nebuleuse.ORM.sql.SentenciaSQL;
import org.json.JSONArray;
import org.json.JSONObject;
import py.com.sistema.facturaproveedordetalle.FacturaProveedorDetalleJSON;



public class FacturaProveedorJSON  {

    Persistencia persistencia = new Persistencia();  
    
    
    public FacturaProveedorJSON ( ) throws IOException  {
        
        
        
    }
      
            

    public JsonObject  lista ( Integer page, String buscar ) {
        
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();                
        JsonObject jsonObject = new JsonObject();
        
        try 
        {               
            
            ResultadoSet resSet = new ResultadoSet();                      
            
            String sql = "";
            String sqlFiltro = "";            
            String sqlOrder = "";
            
            if (buscar == null) {                
                sql = SentenciaSQL.select(new FacturaProveedor());    
            }
            else {                
                sql =  SentenciaSQL.select(new FacturaProveedor());   
                sqlFiltro =  new FacturaProveedorSQL().filtro(buscar);   ;       
            }    
            sqlOrder =  " order by id desc ";            
            
            sql = sql + sqlFiltro + sqlOrder ;          
            
            ResultSet rsData = resSet.resultset(sql, page);                            
            
            List<FacturaProveedor>  lista = new Coleccion<FacturaProveedor>().resultsetToList(
                    new FacturaProveedor(),
                    rsData );     
            String jsonlista = gson.toJson( lista ); 
            
            //JsonArray jsonarrayDatos = new JsonArray();
            JsonArray jsonarrayDatos = JsonParser.parseString(jsonlista).getAsJsonArray();
            
            // paginacipon
            JsonObject jsonPaginacion = new JsonObject();            
            jsonPaginacion = new JsonObjeto().json_paginacion(sql, page);
                        
            // union de partes
            jsonObject.add("paginacion", jsonPaginacion);            
            jsonObject.add("datos", jsonarrayDatos);    
            //jsonObject.add("summary", jsonarraySuma);        
            
            
            resSet.close();
        }         
        catch (Exception ex) {                   
            
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return jsonObject ;         
        }
    }      
    
    
    
    

    
    
    public JSONObject  registro ( Integer codigo ) throws Exception {   
    
       
        FacturaProveedor factura = new FacturaProveedor();        
        
        factura  = (FacturaProveedor) persistencia.filtrarId(factura, codigo);                  
        
        JSONObject jsonVenta = new JSONObject(factura);
        
                
        FacturaProveedorDetalleJSON jsondetalle = new FacturaProveedorDetalleJSON();       
        JSONArray jsonarray = jsondetalle.detalle(codigo);        
        jsonVenta.put("detalle", jsonarray);
        

        return jsonVenta;
    
    }
    
        
    
    
    
        
}
