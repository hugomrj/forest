/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
 
package py.com.sistema.contratista;

import py.com.sistema.oficio.Oficio;



public class Contratista {
    
    private Integer contratista;
    private String nombre;
    private Oficio oficio;

    public Integer getContratista() {
        return contratista;
    }

    public void setContratista(Integer contratista) {
        this.contratista = contratista;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Oficio getOficio() {
        return oficio;
    }

    public void setOficio(Oficio oficio) {
        this.oficio = oficio;
    }
    
}

