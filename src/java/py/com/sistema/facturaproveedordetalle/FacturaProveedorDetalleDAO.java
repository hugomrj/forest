
package py.com.sistema.facturaproveedordetalle;

import java.sql.ResultSet;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.ORM.db.ResultadoSet;
import org.json.JSONArray;
import org.json.JSONObject;
import py.com.sistema.materiales_manoobra.MaterialManoObra;


/**
 *
 * @author hugo
 */
public class FacturaProveedorDetalleDAO {
        
    FacturaProveedorDetalle com = new FacturaProveedorDetalle();
    Persistencia persistencia = new Persistencia();
    
    
    public Integer insertardet (Integer cab,  JSONArray detalleArray ) throws Exception {    

        Integer ret = 0;
        
        // Ahora puedes trabajar con el array detalleArray
        for (int i = 0; i < detalleArray.length(); i++) {
            
            JSONObject detalleObj = detalleArray.getJSONObject(i);
                        
            FacturaProveedorDetalle detalles = new FacturaProveedorDetalle();  
            
            detalles.setId(0);
            detalles.setFactura_proveedor(cab);
            
            
            MaterialManoObra material = new MaterialManoObra();
            material.setId( detalleObj.getInt("material") );
            detalles.setMaterial(material);
            
            
            detalles.setCantidad(detalleObj.getLong("cantidad"));  
            detalles.setPrecio_unitario(detalleObj.getLong("precio_unitario"));  
            detalles.setSubtotal(detalleObj.getLong("subtotal"));  
                       
            detalles = (FacturaProveedorDetalle) this.persistencia.insert(detalles) ; 
            
            ret = i;
        }        

        return ret;
    }
    
    
    public Integer eliminar_detalle (Integer cab ) throws Exception {    
    
        Integer cant = 0;
        
        ResultadoSet resSet = new ResultadoSet();  
        
        String sql = " SELECT id\n" +
                    "  FROM facturas_proveedores_detalles\n" +
                    "  WHERE factura_proveedor  = " + cab ;
        
        
        ResultSet rsData = resSet.resultset(sql);                                    
        while(rsData.next()) 
        {
         // completar codigo
            int id = rsData.getInt("id");
            persistencia.delete(com, id) ; 
            cant++;
        }        
        
        
        return cant;
    }
    
    
}


    
    
















