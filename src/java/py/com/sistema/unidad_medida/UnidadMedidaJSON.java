/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sistema.unidad_medida;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.List;
import nebuleuse.ORM.db.Coleccion;
import nebuleuse.ORM.db.JsonObjeto;
import nebuleuse.ORM.db.ResultadoSet;
import nebuleuse.ORM.sql.SentenciaSQL;


public class UnidadMedidaJSON  {


    
    
    public UnidadMedidaJSON ( ) throws IOException  {
       
        
    }
      
            

    public String  all ( ) {
        
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();                
        JsonObject jsonObject = new JsonObject();
        String jsonlista = ""; 
        
        try 
        {               
            
            ResultadoSet resSet = new ResultadoSet();                      
            
            String sql = "";
            String sqlFiltro = "";            
            String sqlOrder = "";                                
            sql = SentenciaSQL.select(new UnidadMedida());  
            sqlOrder =  " order by unidad_medida";            
            
            sql = sql + sqlFiltro + sqlOrder ;           
            
            ResultSet rsData = resSet.resultset(sql);                            
            
            List<UnidadMedida>  lista = new Coleccion<UnidadMedida>().resultsetToList(
                    new UnidadMedida(),
                    rsData );     
            jsonlista = gson.toJson( lista ); 
            
     
        }         
        catch (Exception ex) {                   
            
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {            
            return jsonlista ;         
        }
    }      
    
    
    
    

    public JsonObject  lista ( Integer page, String buscar ) {
        
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();                
        JsonObject jsonObject = new JsonObject();
        
        try 
        {               
            
            ResultadoSet resSet = new ResultadoSet();                      
            
            String sql = "";
            String sqlFiltro = "";            
            String sqlOrder = "";
            
            if (buscar == null) {                
                sql = SentenciaSQL.select(new UnidadMedida());    
            }
            else {                
                sql =  SentenciaSQL.select(new UnidadMedida());   
                sqlFiltro =  new UnidadMedidaSQL().filtro(buscar);   ;       
            }    
            sqlOrder =  " order by unidad_medida ";            
            
            sql = sql + sqlFiltro + sqlOrder ;           
            
            ResultSet rsData = resSet.resultset(sql, page);                            
            
            List<UnidadMedida>  lista = new Coleccion<UnidadMedida>().resultsetToList(
                    new UnidadMedida(),
                    rsData );     
            String jsonlista = gson.toJson( lista ); 
            
            //JsonArray jsonarrayDatos = new JsonArray();
            JsonArray jsonarrayDatos = JsonParser.parseString(jsonlista).getAsJsonArray();
            
            // paginacipon
            JsonObject jsonPaginacion = new JsonObject();            
            jsonPaginacion = new JsonObjeto().json_paginacion(sql, page);
                        
            // union de partes
            jsonObject.add("paginacion", jsonPaginacion);            
            jsonObject.add("datos", jsonarrayDatos);    
            //jsonObject.add("summary", jsonarraySuma);        
            
            
            resSet.close();
        }         
        catch (Exception ex) {                   
            
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return jsonObject ;         
        }
    }      
    
    
    
        
    
        
}
