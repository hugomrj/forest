/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package py.com.sistema.unidad_medida;

import java.sql.SQLException;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.util.List;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.UriInfo;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.MatrixParam;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.seguridad.Autentificacion;




/**
 * REST Web Service
 * @author hugo
 */


@Path("unidadesmedidas")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)



public class UnidadMedidaWS {

    @Context
    private UriInfo context;    
    private Persistencia persistencia = new Persistencia();   
    private Autentificacion autorizacion = new Autentificacion();
    private Gson gson = new Gson();          
    private Response.Status status  = Response.Status.OK;
    
    UnidadMedida com = new UnidadMedida();       
                         
    public UnidadMedidaWS() {
    }

    


 
    @GET    
    @Path("/all") 
    public Response all (
        @HeaderParam("token") String strToken
    ) {
        
        
        try {                    
           
            //if (autorizacion.verificar(strToken))
            if (true)
            {                
                //autorizacion.actualizar();                                
                
                String jsonString = new UnidadMedidaJSON().all();
                
                
                return Response
                        .status(Response.Status.OK)
                        .entity(jsonString)
                        //.header("token", autorizacion.encriptar())                        
                        .build();                       
            }
            else
            {                
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();    
            }        
        }   
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(ex.getMessage())
                    .header("token", null)
                    .build();                                        
        }          

        
    }    
    
    
    
    @GET    
    public Response lista ( 
            @HeaderParam("token") String strToken,
            @MatrixParam("q") String q,            
            @QueryParam("page") Integer page) {
        
        
            if (page == null) {                
                page = 1;
            }

        try {                    
           
            //if (autorizacion.verificar(strToken))
            if (true)
            {                
                // autorizacion.actualizar();                                
                
                JsonObject jsonObject ;                                                
                if (q == null) {                                
                    jsonObject = new UnidadMedidaJSON().lista(page, null);
                } 
                else{
                    jsonObject = new UnidadMedidaJSON().lista(page, q);
                }                
                
                
                return Response
                        .status(Response.Status.OK)
                        .entity(jsonObject.toString())
                        //.header("token", autorizacion.encriptar())
                        .build();                       
            }
            else{
                
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();   
            }        
        }     
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", null)
                    .build();                                        
        }      
    }    
    
      
    
        

    
    
    
    
    
    @GET
    @Path("/{id}")
    public Response get(     
            @HeaderParam("token") String strToken,
            @PathParam ("id") Integer id ) {
                     
        try 
        {                  
            if (true)
            //if (autorizacion.verificar(strToken))
            {
                //autorizacion.actualizar();       
                
                this.com  = (UnidadMedida) persistencia.filtrarId(this.com, id);  
                
                String json = gson.toJson(this.com);    
                
                                
                if (this.com == null){
                    this.status = Response.Status.NO_CONTENT;                           
                }

                
                return Response
                        .status( this.status )                        
                        .entity(json)
                        .header("token", autorizacion.encriptar())
                        .build();       
            }
            else{
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();   
            }
        
        }     
        catch (Exception ex) {            
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(ex.getMessage())
                    .header("token", autorizacion.encriptar())
                    .build();           
        }          
    }    
      
        
    
    
    

    


 
 
    @POST
    public Response add( 
            @HeaderParam("token") String strToken,
             String json ) {
                     
        
        try {                    
           
            if (true)
            //if (autorizacion.verificar(strToken))
            {                
                //autorizacion.actualizar();    
                
                UnidadMedida req = gson.fromJson(json, UnidadMedida.class);                   
                
                this.com = (UnidadMedida) persistencia.insert(req);                 
                
                if (this.com == null){
                    this.status = Response.Status.NO_CONTENT;
                }                                
                
                json = gson.toJson(this.com);
                
                return Response
                        .status(this.status)
                        .entity(json)
                        .header("token", autorizacion.encriptar())
                        .build();                       
            }
            else
            {                                
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();                   
                
            }
        
        }     
        catch (Exception ex) {
            
            System.err.println(ex.getMessage());
            
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(ex.getMessage())
                    .header("token", autorizacion.encriptar())
                    .build();                                        
        }        

    }    
 
    
    

    @PUT    
    @Path("/{id}")    
    public Response edit (            
            @HeaderParam("token") String strToken,
            @PathParam ("id") Integer id,
            String json 
            ) {

        try {                    
           
            //if (autorizacion.verificar(strToken))
            if (true)
            {                
                //autorizacion.actualizar();    
                
                UnidadMedida req = new Gson().fromJson(json, UnidadMedida.class);                                      
                req.setUnidad_medida(id);
                
                this.com = (UnidadMedida) persistencia.update(req);
                json = gson.toJson(this.com);    
            
                return Response
                        .status(Response.Status.OK)
                        .entity(json)
                        .header("token", autorizacion.encriptar())
                        .build();       
            }
            else{    
            
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();                   
                
            }
        
        }     
        
        
        
        catch (SQLException ex) {   
            return Response
                    .status(Response.Status.BAD_GATEWAY)
                    .entity(ex.getMessage())
                    .header("token", autorizacion.encriptar())
                    .build();    
        }        
                
        
        
        catch (Exception ex) {

            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", autorizacion.encriptar())
                    .build();         
        }        
        
        
        
    }    
    
    
        
        
    
    

    @DELETE  
    @Path("/{id}")    
    public Response delete (            
            @HeaderParam("token") String strToken,
            @PathParam ("id") Integer id) {
            
        try {                    
           
            //if (autorizacion.verificar(strToken))
            if (true)                
            {                
                //autorizacion.actualizar();    
            
                Integer filas = 0;
                filas = persistencia.delete(this.com, id) ;
                                
                if (filas != 0){
                    
                    return Response
                            .status(Response.Status.OK)
                            .entity(null)
                            .header("token", autorizacion.encriptar())
                            .build();                       
                }
                else{                    
                    
                    return Response
                            .status(Response.Status.NO_CONTENT)
                            .entity(null)
                            .header("token", autorizacion.encriptar())
                            .build();          
                    
                    
                }
            }
            else
            {  
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();              
            }        
        } 

        catch (Exception ex) {            
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(ex.getMessage())
                    .header("token", autorizacion.encriptar())
                    .build();           
        }  
        
    }    
        
    
    
        
        
    

    
    
    
}





