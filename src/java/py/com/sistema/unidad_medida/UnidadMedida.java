/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.sistema.unidad_medida;

/**
 *
 * @author hugo
 */
public class UnidadMedida {
    
    private Integer unidad_medida;
    private String descripcion;    

    public Integer getUnidad_medida() {
        return unidad_medida;
    }

    public void setUnidad_medida(Integer unidad_medida) {
        this.unidad_medida = unidad_medida;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    
}
