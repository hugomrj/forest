/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.usuario;

import java.io.IOException;
import java.sql.ResultSet;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.ORM.db.ResultadoSet;
import py.com.base.sistema.rol.RolSQL;



/**
 *
 * @author hugo
 */

public class UsuarioExt extends Usuario{
    
    
    
    public void extender() throws IOException {
        
    }

    
    // campo auxliar de tabla roles
    public Integer getOficina (String user) throws Exception {
        
        Persistencia persistencia = new Persistencia();
        Integer ret = 0;
    
        Integer usercode = Integer.parseInt(user);
        
        String sql = new RolSQL().getuxiliar(usercode);

        ret = persistencia.ejecutarSQL ( sql, "auxiliar") ;

        return ret;    
    }
    
    
            
}
