/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.config.REST;

import java.util.Set;
import jakarta.ws.rs.core.Application;

/**
 *
 * @author hugo
 */


@jakarta.ws.rs.ApplicationPath("api")
public class ApplicationVersion1 extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }
    
    

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        
        
        resources.add(py.com.base.sistema.rol.RolWS.class);
        resources.add(py.com.base.rol_selector.RolSelectorWS.class);
        resources.add(py.com.base.selector.SelectorWS.class);        
        resources.add(py.com.base.usuario.UsuarioWS.class);
        resources.add(py.com.bass.usuario_rol.UsuarioRolWS.class);
        
        
        resources.add(py.com.sistema.oficio.OficioWS.class);
        resources.add(py.com.sistema.proveedor.ProveedorWS.class);
        resources.add(py.com.sistema.contratista.ContratistaWS.class);
        resources.add(py.com.sistema.obrarealizada.ObraRealizadaWS.class);
        resources.add(py.com.sistema.materiales_manoobra.MaterialManoObraWS.class);        
        
        resources.add(py.com.sistema.unidad_medida.UnidadMedidaWS.class);
        
        
        resources.add(py.com.sistema.tipo_usuario.TipoUsuarioWS.class);
        resources.add(py.com.sistema.tipo_compra.TipoCompraWS.class);
        resources.add(py.com.sistema.estado_pago.EstadoPagoWS.class);
        resources.add(py.com.sistema.tipo_cheque.TipoChequeWS.class);
        
        resources.add(py.com.sistema.facturaproveedor.FacturaProveedorWS.class);
        resources.add(py.com.sistema.facturacontratista.FacturaContratistaWS.class);
        
        
        resources.add(py.com.sistema.consulta.ConsultaWS.class);
        
    }
    
}
