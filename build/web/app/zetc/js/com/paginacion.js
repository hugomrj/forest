/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

 
var paginacion_2 = 
{  
    pagina:     1,        
    total_registros: 0,
    
    lineas:       12,
    cantidad_paginas:  0,
    
    pagina_inicio : 1,
    pagina_fin : 0, 
    bloques:    12,
    
    html: "",
    
    setTotalRegistros: function() {        
        paginacion_2.total_registros = sessionStorage.getItem('total_registros');
        
    },
       
        
    ini: function() {
 
        paginacion_2.setTotalRegistros();
    

        paginacion_2.cantidad_paginas = paginacion_2.total_registros / paginacion_2.lineas;
        paginacion_2.cantidad_paginas = Math.trunc(paginacion_2.cantidad_paginas);
        
       
        
        var calresto  = paginacion_2.cantidad_paginas * paginacion_2.lineas;
        if (calresto != paginacion_2.total_registros) {
            paginacion_2.cantidad_paginas = paginacion_2.cantidad_paginas + 1;
        }
 
 
        if ((paginacion_2.cantidad_paginas * paginacion_2.lineas ) < paginacion_2.total_registros){
            paginacion_2.cantidad_paginas++;
        }        

        if (paginacion_2.bloques > paginacion_2.cantidad_paginas) {
            paginacion_2.pagina_inicio = 1;
            paginacion_2.pagina_fin = paginacion_2.cantidad_paginas;
        }        
        else{
            if (paginacion_2.bloques == paginacion_2.cantidad_paginas) {
                paginacion_2.pagina_inicio = 1;
                paginacion_2.pagina_fin = paginacion_2.bloques;
            }    
            else{ 
                if (paginacion_2.bloques  < paginacion_2.cantidad_paginas) {

                    if (paginacion_2.pagina - (paginacion_2.bloques / 2) < 1) {
                        paginacion_2.pagina_inicio = 1;
                        paginacion_2.pagina_fin = paginacion_2.pagina + paginacion_2.bloques;
                    }
                    else{
                        paginacion_2.pagina_inicio = paginacion_2.pagina - (paginacion_2.bloques / 2);
                    }

                    if (paginacion_2.pagina + (paginacion_2.bloques / 2) > paginacion_2.cantidad_paginas )                        {
                        paginacion_2.pagina_inicio = paginacion_2.cantidad_paginas - paginacion_2.bloques;
                        paginacion_2.pagina_fin = paginacion_2.cantidad_paginas;
                    }
                    else{
                        paginacion_2.pagina_fin =  paginacion_2.pagina + (paginacion_2.bloques / 2);
                    }    
                }
            }    
        }                 
    },
                
        
    gene: function() {       
        
        paginacion_2.ini();
        


//paginacion.total_registros  = 16



        
        if (paginacion_2.total_registros == 0){
            return "";
        }
        
        if (paginacion_2.cantidad_paginas == 1){
            paginacion_2.pagina = 1;
        }
        
        paginacion_2.html = "<ul data-paginaactual=\"1\" id=\"paginacion\">";
     

        if (paginacion_2.pagina != paginacion_2.pagina_inicio){            
            paginacion_2.html += "<li data-pagina=\"ant\" >" ;
            
            paginacion_2.html +=      "<a href=\"javascript:void(0);\"id =\"ant\">" ;
            paginacion_2.html +=          "anterior" ;
            paginacion_2.html +=      "</a>" ;
            
            paginacion_2.html += "</li>";
        }
   
  
   
        for (i = paginacion_2.pagina_inicio; i <= paginacion_2.pagina_fin; i++) {   
                        
            if (paginacion_2.pagina === i)
            {                
                paginacion_2.html += "<li class=\"actual\" data-pagina=\"act\">" ;
                paginacion_2.html += "<a href=\"javascript:void(0);\" > " ;
                paginacion_2.html += i ;
                paginacion_2.html += "</a>" ;
                paginacion_2.html += "</li>" ;
            }
            else
            {                
                paginacion_2.html += "<li data-pagina= \"pag" + i + "\"  >" ;
                paginacion_2.html += "<a href=\"javascript:void(0);\" id =pag"+ i +"> " ;
                paginacion_2.html += i ;
                paginacion_2.html += "</a>" ;
                paginacion_2.html += "</li>" ;                
                
            }
        }
   
        if (paginacion_2.pagina != paginacion_2.pagina_fin){            
            paginacion_2.html += "<li data-pagina=\"sig\" >" ;
            
            paginacion_2.html +=      "<a href=\"javascript:void(0);\"id =\"sig\">" ;
            paginacion_2.html +=          "siguiente" ;
            paginacion_2.html +=      "</a>" ;
            
            paginacion_2.html += "</li>";
        }
        paginacion_2.html += " </ul>"
        return paginacion_2.html;
        
    },

    
    
    
    
    
    move: function( obj, busca, fn) {     


        var listaUL = document.getElementById( obj.tipo + "_paginacion" );
        var uelLI = listaUL.getElementsByTagName('li');
        
        for (var i=0 ; i < uelLI.length; i++)
        {
            var datapag = uelLI[i].dataset.pagina;     

            if (!(datapag == "act"  || datapag == "det"  ))
            {
                uelLI[i].addEventListener ( 'click',
                    function() {                                      
                        
                        switch (this.dataset.pagina)
                        {
                           case "sig": 
                                   paginacion_2.pagina = parseInt(paginacion_2.pagina) +1;
                                   break;                                                                          

                           case "ant":                                     
                                   paginacion_2.pagina = parseInt(paginacion_2.pagina) -1;
                                   break;

                           default:  
                                   paginacion_2.pagina = this.childNodes[0].innerHTML.toString().trim();
                                   break;
                        }
                        paginacion_2.pagina = parseInt( paginacion_2.pagina , 10);                                                                       
                                                
                        tabla.refresh( obj, paginacion_2.pagina, busca, fn  ) ;

                        
                    },
                    false
                );                
            }            
        }           
    
    
    
    },
    

    
    
    
    
    
    
};
 






    