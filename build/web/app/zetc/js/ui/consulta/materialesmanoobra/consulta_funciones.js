



function main_form (obj) {    

    fetch('htmf/cab.html')
      .then(response => {
        return response.text();
      })
      .then(data => {
        
        document.getElementById('arti_form').innerHTML = data ;
        
        
        var obra = new ObraRealizada();
        obra.combobox("obra_realizada", 1);
        
        lista_det();
        
        
        
        var btn_consultar = document.getElementById('btn_consultar');
        btn_consultar.onclick = function()
        {                                  

            var obraValor = document.getElementById('obra_realizada');
            var selectedValue = obraValor.value;

            // Comprobar si el valor está vacío y asignar cero si es el caso
            if (selectedValue === "") {
                selectedValue = "0";
            }

            // Imprimir el valor seleccionado
            console.log(selectedValue);


            arasa.ajax.api = arasa.html.url.absolute()  +  "/api/consulta/consulta1/" + selectedValue;        
            arasa.ajax.promise.async("get").then(result => {


                console.log(result.responseText);
                var ojson = JSON.parse( result.responseText ) ;    
             

                arasa.tabla.json = JSON.stringify(ojson['datos']);
                arasa.tabla.id = "consulta_tabla";
                arasa.tabla.linea = "codigo";
                arasa.tabla.tbody_id = "consulta-tb";
                arasa.tabla.campos = ['codigo', 'materialmano', 'uni_medida', 'cantidad', 'total'];
                        
                        

            //    tabla.ini(obj);
                arasa.tabla.gene();   

                var obj = new Object();
                obj.tablaformat = ['N', 'C', 'C', 'N', 'N'];                       
                arasa.tabla.formato(obj);




                // Obtener el valor de la suma general
                var sumaGeneral = ojson.suma.total_general;
                console.log(sumaGeneral);
                document.getElementById( "total_general" ).innerHTML =  fmtNum(sumaGeneral.toString()); 
                

            });




        };          

        
        
        
        
        
        // form_ini (obj);
            
      })

};







function form_ini (obj) {    

    

    var btn_xlsx = document.getElementById( 'btn_xlsx');
    btn_xlsx.onclick = function()
    {  

        var fecha_desde = document.getElementById('fecha_desde');
        var fecha_hasta = document.getElementById('fecha_hasta');

        if (fecha_desde.value != "" &&  fecha_hasta.value != "" ){        

            loader.inicio();

            var url = html.url.absolute() + '/api/consolidadogeneral_qry/consulta1/xlsx/'                
                +';fe1='+document.getElementById("fecha_desde").value 
                +';fe2='+document.getElementById("fecha_hasta").value 


            // Use XMLHttpRequest instead of Jquery $ajax
            xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {

                if (xhttp.readyState === 4 && xhttp.status === 200) {

                    var file_name = "consolidado_general.xlsx" ; 

                    // Trick for making downloadable link
                    var a = document.createElement('a');
                    a.href = window.URL.createObjectURL(xhttp.response);
                    a.download = file_name;
                    a.click();
                    loader.fin();

                }
            };
            // Post data to URL which handles post request
            xhttp.open("GET", url);
            xhttp.setRequestHeader("Content-Type", "application/json");

            // You should set responseType as blob for binary responses
            xhttp.responseType = 'blob';
            xhttp.setRequestHeader("token", localStorage.getItem('token'));           
            xhttp.send();

        }
    }



          
};








function lista_det(){    
    
    fetch('htmf/det.html')
      .then(response => {
        return response.text();
      })
      .then(data => {
        
        document.getElementById('arti_det').innerHTML = data ;
        
        // form_ini (obj);
            
            
      })    
    
    
    
    

}  
